class Node:
    def __init__(self, value, lvl):
        """
        A simple tree data structure.

        Parameters
        ----------
        node: obj

        """
        self.value = value
        self.lvl = int(lvl) if isinstance(lvl, str) else lvl
        self.parent = None
        self.children = []

    def add_children(self, child):
        """
        Add a child node

        Returns
        -------
        self
        """

        child.parent = self
        self.children.append(child)
        return self

    def get_children(self):
        """
        List of successor nodes

        Returns
        -------
        children: list[Node]
        """

        return self.children

    def get_parent(self):
        """
        Parent of the current node

        Returns
        -------
        parent: Node
        """

        return self.parent

    def is_leaf(self):
        """
        Is the current node the leaf node

        Returns
        -------
        no_child: Bool
            Return True if the node has no children
        """

        return len(self.children) == 0

    def is_root(self):
        """
        Is the current node the root node of a Tree

        Returns
        -------
        no_parent: Bool
            Return True if current node has no parent node
        """

        return self.lvl == -1

    def __repr__(self, level=0):
        """
        Pretty print tree when called

        Parameters
        ----------
        level: int
            Tree level to print from.

        Returns
        -------
        None
        """

        ret = "\t" * level + repr(self.value) + "\n"
        for child in self.children:
            ret += child.__repr__(level + 1)
        return ret

