"""
This program contains the MetaModel used to represent COBOL statements.
It enables cataloging cobol statements
"""

from __future__ import print_function
import os
import re
import sys
from ipdb import set_trace

"Update python path"
root = os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct'))
if root not in sys.path:
    sys.path.append(root)
from utils.nlp.guesstimate import NoisyChannelEstimation

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


class Statement:
    """
    Create the Statement object to hold all the COBOL
    """

    "Let's categorize COBOL verbs"
    VERBS = {
        "MOVE": ["MOVE"],
        "ASSIGNMENT": ["SET"],
        "PERFORM": ["PERFORM"],
        "CONDITIONAL": ["IF", "ELSE"],
        "DATATYPE": ["STRING", "NUMERIC"],
        "COMPUTATIONAL": ["ADD", "COMPUTE"],
        "PROGRAM_FLOW": ["STOP", "EXIT", "CONTINUE", "PERFORM"],
        "INPUT": ["ACCEPT", "OPEN", "READ"],
        "OUTPUT": ["CLOSE", "DISPLAY", "WRITE"]
    }

    f = open(os.path.join(root, "data", "cobol_reserved_keywords.txt"), 'r')
    RESERVED_KEYW = [line.rstrip("\n") for line in f.readlines()]

    def __init__(self, raw_cobol_text):
        """
        This instance is usually "called" as a function. When that happens, populate the
        the attributes of the instance and return a fully populated version of the instance.

        Parameters
        ----------
        raw_cobol_text: str
            Cobol statement encoded as a string

        Returns
        -------
        None: None
            Returns None
        """

        self.id = id(self)
        self.perform_tag = []
        self.description = ""
        self.output_string = []
        self.statement_type = []
        self.variables_type = []
        self.est = NoisyChannelEstimation()

        self.cobol_statement = self._cobol_statement_split(raw_cobol_text)
        "Gather all variables"
        self.all_variables = [
            word for word in self.cobol_statement if word not in Statement.RESERVED_KEYW and not word == "STRING" and not word in self.output_string and not word.isdigit()]

        "Gather all keywords"
        self.all_keywords = [
            word for word in self.cobol_statement if not word in self.all_variables]

        "Call a pair of setter methods"
        self._set_statement_type()
        self._set_variables_category()
        self._describe_sentence()

    def _cobol_statement_split(self, raw_cobol_statement):
        """
        If the cobol statement contains strings to be displayed, this procedure captures those strings for further
        processing

        Parameters
        ----------
        raw_cobol_statement: str
            The string of cobol statement

        Returns
        -------
        parts: list
            A list of parts in the cobol statement
        """

        idx = [i for i, stat in enumerate(raw_cobol_statement) if "'" in stat]
        if not len(idx):
            parts = [word.replace('.', "") for word in raw_cobol_statement.split()]
        else:
            self.output_string = ["".join(raw_cobol_statement[idx[0]:idx[1] + 1])]
            parts = raw_cobol_statement[:idx[0]].split() + self.output_string + raw_cobol_statement[idx[1] + 1:].split()
        return parts

    def _set_statement_type(self):
        """
        Assigns every statement one (or more) types.

        Parameters
        ----------
        N/A: None
            Takes no input arguments

        Return
        ------
        N/A: None
            Nothing is returned

        Notes
        -----
        The types are predefined using the variable called VERBS. In the future, we may choose to 
        use a more "correct" way to do this. For now, however, this should suffice.

        If you are verifying or reimplementing these methods, please make sure to update (or correct) 
        the predefined assignments. This plays a big part in how the rest of the system works.
        """

        counts = dict()
        for verb_key, verb_values in self.VERBS.items():
            counts.update(
                {verb_key: len(set(self.all_keywords).intersection(verb_values))})

        for k, v in counts.items():
            if v > 0:
                self.statement_type.append(k)

        if "PERFORM" in self.statement_type or self.cobol_statement == self.all_variables:
            try:
                self.perform_tag = self.all_variables[0].replace(".", "")
            except IndexError:
                pass

    def _set_variables_category(self):
        """
        Groups Variables in the Statement in to types

        PARAMETERS
        ----------
        N/A:
            No input parameters
        RETURNS
        -------
        N/A:
            No returns

        NOTES
        -----
        We have 4 groups here:
            - All variables: (self.all_variables): We store all the variables here.
            - Conditional variables: (self.conditional_variables): Variables that follow IF/ELSE/...
            - Input Output Variables: (self.inout_variables): Variable that do I/O operations.
            - Computational Variables: (self.compute_variables): Variables that partake in computations.
        """

        if 'CONDITIONAL' in self.statement_type:
            self.variables_type.append('CONDITIONAL')

        if 'INPUT' in self.statement_type:
            self.variables_type.append('INPUT')

        if 'OUTPUT' in self.statement_type:
            self.variables_type.append('OUTPUT')

        if 'COMPUTATIONAL' in self.statement_type:
            self.variables_type.append('COMPUTATIONAL')

        else:
            self.variables_type.append('MISC.')

    def get_all_variables(self):
        """
        This method returns all variables in a dictionary (json) format.

        Parameters
        ----------
        N/A: None
            This method takes no input variables.

        Return
        ------
        variables: dict
            A dictionary of all the variables.
        """

        return self.all_variables

    def get_statement_type(self):
        """
        This method returns the statement type.

        Parameters
        ----------
        N/A: None
            This method takes no input variables.

        Return
        ------
        variables: string
            Statement type
        """

        return self.statement_type

    def _describe_sentence(self):
        """
        Takes a raw cobol statement and creates a natural language description of the sentence

        Returns
        -------
        description: str
            A description text
        """

        if 'MOVE' in self.statement_type:
            if len(self.output_string):
                self.description += "Copy string {} to variable ".format(self.output_string[0])
            else:
                self.description += "Move a numeric value to variable ".format(self.output_string)

            full_word = self.est.full_word_guess(self.get_all_variables()[0])
            self.description += str(full_word)

        if 'OUTPUT' in self.statement_type:
            if 'WRITE' in self.cobol_statement:
                full_word_1 = self.est.full_word_guess(self.get_all_variables()[1])
                full_word_2 = self.est.full_word_guess(self.get_all_variables()[0])
                self.description += "Copy content of {} to {}".format(full_word_1, full_word_2)
            else:
                self.description += 'Display '
                full_word = self.est.full_word_guess(self.get_all_variables()[0])
                self.description += str(full_word)

        if "COMPUTATIONAL" in self.statement_type:
            if 'ADD' in self.cobol_statement:
                full_word = self.est.full_word_guess(self.get_all_variables()[0])
                digit = [eval(word) for word in self.cobol_statement if word.isdigit()]
                self.description += "Increment value of {} by {}".format(full_word, digit)

            if 'COMPUTE' in self.cobol_statement:
                set_trace()

class BasicBlock:
    def __init__(self, statement):
        self.raw_stmt = statement
        striped_statement = re.sub(r'\[|\]|:', '', statement)
        self.statements = [Statement(s) for s in re.sub(r'\\n', ',', striped_statement).split(",")]

    def get_basicblock_type(self):
        pass
