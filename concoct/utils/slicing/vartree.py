import os
import sys
from pathlib import Path

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from utils.data_structures.tree import Node

class VarHierarchy:
    def __init__(self, cobol_file):
        """
        Generate a variable hierarchy tree from COBOL code

        Parameters
        ----------
        cobol_file: str or (from pathlib PurePath and PosixPath)
            Path to the cobol code
        """
        self.cobol_file = cobol_file
        self.raw_script = self.get_raw_cobol_script()

    def get_raw_cobol_script(self):
        """
        Reads a COBOL file and returns a raw text of the cobol code

        Parameters
        ----------
        N/A: None
            Takes no input

        Returns
        -------
        N/A: None
            Nothing is returned
        """

        with open(self.cobol_file, 'r') as script:
            text = script.readlines()

        return text

    def working_storage_section(self):
        """
        Gather sentences in the working storage section

        Parameters
        ----------
        N/A: None
            Takes no input variables

        Returns
        -------
        ws_vars: List
            A raw list of variables that lie in the working storage
        """

        for idx, line in enumerate(self.raw_script):
            if 'WORKING-STORAGE SECTION.' in line:
                start = idx
            elif "PROCEDURE DIVISION." in line:
                end = idx

        raw_ws_vars = self.raw_script[start+1:end]
        ws_vars = [tuple(var.rstrip("\n").split()[:2]) for var in raw_ws_vars]
        ws_vars = list(filter(lambda elem: len(elem) > 1 and elem[0].isdigit(), ws_vars))
        return ws_vars

    def generate_var_tree(self):
        """
        Creates a dictionary representing variable hierarchy

        Parameters
        ----------
        N/A: None
            Takes no input variables

        Returns
        -------
        var_hierarchy: <utils.data_structures.tree.Node>
            A node object representing levels and variables in each level.

        Notes
        -----

        A sample variable hierarchy look like this:

        'Working Storage Section'
            'WS-AREA.'
                'WS-MSG'
                'WS-DATE.'
                    'WS-DATE-YY'
                    'WS-DATE-MM'
                    'WS-DATE-DD'
                    'REPORT-STATUS-2'
            ...
        """
        ws_vars = self.working_storage_section()
        "Build a tree using NetworkX"
        var_tree = Node(value="Working Storage Section", lvl=-1)
        previous = var_tree

        "An O(n) procedure to construct the tree"
        for lvl, value in ws_vars:
            current = Node(value, lvl)

            if current.lvl > previous.lvl:
                previous.add_children(current)

            elif current.lvl == previous.lvl:
                parent = previous.parent
                parent.add_children(current)

            elif current.lvl < previous.lvl:
                while previous.lvl >= current.lvl:
                    previous = previous.parent
                previous.add_children(current)

            previous = current

        return var_tree
