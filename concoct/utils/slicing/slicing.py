"""
This class is used to generate slices from a precomputed CFG (Control Flow Graph)
file
"""

from __future__ import division
from __future__ import print_function
import os
import sys
import networkx as nx
from pathlib import Path

"Update python path"
root = os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct'))
if root not in sys.path:
    sys.path.append(root)

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"

from utils.data_structures.metamodel import BasicBlock


class Slice:
    def __init__(self, dot_file=None):
        self.dot_file = dot_file

    @staticmethod
    def get_slice_from_file(filename):
        """
        Read slices from a pre-computed slice file.

        Parameters
        ----------
        filename: str or PurePath/PosixPath
            A pathname string pointing to the slice file

        Returns
        -------
        slices: list[<utils.data_structures.metamodel.BasicBlock>]
            A list of slices as
        """
        if isinstance(filename, str):
            filename = Path(filename)

        with open(filename, 'r') as f:
            lines = f.readlines()

        prev = -1
        slices = []
        for i, slice_text in enumerate(lines):
            if "===" in slice_text:
                slices.append(BasicBlock(",".join(lines[prev+1:i])))
                prev = i

        return slices

    def _dot_to_nx(self):
        """
        A private method to read a dot file as networkx graph

        Input Parameters
        ----------------
        N/A:
            This function takes no input parameters

        Returns
        -------
        graph: <nx.classes.multidigraph.MultiDiGraph>
            A NetworkX graph
        """
        graph = nx.drawing.nx_agraph.read_dot(self.dot_file)
        return graph

    @staticmethod
    def _get_sink_and_source_nodes(graph):
        """
        This method returns all possible sink nodes

        Parameters
        ----------
        graph: <nx.classes.multidigraph.MultiDiGraph>
            A full NetworkX graph

        Returns
        -------
        sinks: List[<nx.Graph.nodes>]
            A list of all sinks
        """
        nodes = list(graph.nodes)
        sink_nodes = []
        source_nodes = []

        "The next few lines encode the strings into container for easy search/retrieval"
        for node in nodes:
            "Find output like statements (e.g. write to file, display message, etc.)"
            # Create a basic block object to hold all the statements.
            block = BasicBlock(node)
            if any(['OUTPUT' in stat.statement_type for stat in block.statements]):
                "So, if the statement has any output variables, that will be our sink."
                "ASIDE: Look up python's 'any' built-in method, it's pretty nifty."
                sink_nodes.append(block)
            if any(['INPUT' in stat.statement_type for stat in block.statements]):
                "If the statement has any output variables, that will be our sink."
                source_nodes.append(block)

        return sink_nodes, source_nodes

    @staticmethod
    def _trace_source_to_sinks(graph, source, sink_nodes):
        """
        Give a sink, find the corresponding source(s)

        Parameters
        ----------
        graph: <nx.classes.multidigraph.MultiDiGraph>
            A full NetworkX graph
        source: str
            A source node. This is usually a string.
        sink_nodes: list[str]
            A list of sink nodes. This is usually a list of strings.

        Returns
        -------
        sinks: List[<nx.Graph>]
            A list of subgraphs that constitute paths between a source and a sink

        Notes
        -----

        Notice the use of networkx's in-built method called <nx.all_simple_paths>
        See the documentation (here: https://goo.gl/YHinB3) for more details.


        This method is used to discover the path between a given source and a sink.
        Again, I have made some inherent assumptions on how the slicing works. I may 
        be wrong. So, please use/extend with care.
        """

        source_to_sink_paths = []
        for sink in sink_nodes:

            paths = [pp for pp in nx.all_simple_paths(
                graph, source.raw_stmt, sink.raw_stmt)]

            for path in paths:
                source_to_sink_paths.append([BasicBlock(p) for p in path])

        return source_to_sink_paths

    def get_slice(self, graph=None):
        """
        This method is used to obtain slices from NetworkX graph

        Input Parameters
        ----------------
        graph: <nx.classes.multidigraph.MultiDiGraph>
            (OPTIONAL) A full NetworkX graph

        Returns
        -------
        slices: List
            A list of slices as NetworkX graphs

        Notes
        -----
          + If no input graph is given, this procedure assumes that the dot provided as a
            part of input definition will be the graph. Provide a graph to override this
            functionality.

          + This procedure follows a 2 step process
                1.  We identify a set of 'sinks'. Sinks are vertices on the graphs that represent
                    some kind of output (ex. write to file, display message, etc.)
                2.  We traceback node until we reach (a) beginning of a function definition, (b) some
                    kind of input action (ex. open file, read, etc.)
        """
        if graph is None:
            graph = self._dot_to_nx()

        sinks, sources = self._get_sink_and_source_nodes(graph)

        slices = []
        for source in sources:
            slices.extend(self._trace_source_to_sinks(
                graph, source=source, sink_nodes=sinks))

        return slices
