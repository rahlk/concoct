from collections import defaultdict
import re
import numpy as np
import pandas as pd
from ipdb import set_trace
from nltk import bigrams, trigrams
from collections import Counter
from nltk.stem.lancaster import LancasterStemmer


class LanguageNgramModel:
    def __init__(self, order=5, smoothing=0.9, recursive=0.001):
        """
        This model attempts to remember the sequence of letters that follow each other for a language.
        That is, given a corpus of words, this constructor creates a model that learns all the n-grams
        of the corpus.

        Parameters
        ----------
        order: int
            The number of characters the model remembers, i.e., N-1.
        smoothing: float
            Used to control the stability of the model.
        recursive: float
            Weight of the model of one order less.

        Other Parameters
        ----------------
        counter_: dict
            Used to store the N-Grams.
        vocabulary_: set
            A set of characters the model is aware of.
        child_: self
            A recusive variable of type self.
        """

        self.order = order
        self.smoothing = smoothing
        self.recursive = recursive
        self.counter_ = defaultdict(lambda: Counter())
        self.vocabulary_ = set()
        self.child_ = self

    def fit(self, corpus):
        """
        Compute the frequency of all n-grams in the text

        Parameters
        ----------
        corpus: str
            A string of text
        """

        self.corpus = corpus

        for i, token in enumerate(corpus[self.order:]):
            context = corpus[i:(i + self.order)]
            self.counter_[context][token] += 1
            self.vocabulary_.add(token)

        self.vocabulary_ = sorted(list(self.vocabulary_))

        if self.recursive > 0 and self.order > 0:
            self.child_ = LanguageNgramModel(order=self.order - 1, smoothing=self.smoothing, recursive=self.recursive)
            self.child_.fit(corpus)

    def get_counts(self, context):
        """
        Compute the frequency of all the letters that may follow the current context

        Parameters
        ----------
        context: str
            A string consisting of the last self.order characters

        Returns
        -------
        freq: pandas.Series
            A vector of letter conditional frequencies
        """

        if self.order:
            local = context[-self.order:]
        else:
            local = ""

        freq_dict = self.counter_[local]
        freq = pd.Series(index=self.vocabulary_)

        for i, token in enumerate(self.vocabulary_):
            freq[token] = freq_dict[token] + self.smoothing

        if self.recursive > 0 and self.order > 0:
            child_freq = self.child_.get_counts(context) * self.recursive
            freq += child_freq

        return freq

    def predict_proba(self, context):
        """
        Compute the probability of all the letters that may follow the context.

        Parameters
        ----------
        context: str
            A string consisting of the last self.order characters

        Returns
        -------
        freq: pandas.Series
            A vector of letter conditional frequencies
        """
        counts = self.get_counts(context)
        freq = counts / counts.sum()
        return freq

    def single_log_proba(self, context, continuation):
        """
        Compute the log-prob of a specific continuation given the context.

        Parameters
        ----------
        context: str
            Known beginning of the phrase
        continuation: str
            A likely end of the phrase

        Returns
        -------
        log_prob: float
            The log-probability

        """

        log_prob = 0.0
        for token in continuation:
            log_prob += np.log(self.predict_proba(context)[token])
            context += token
        return log_prob

    def single_proba(self, context, continuation):
        """
        Compute the probability of a specific continuation given the context.

        Parameters
        ----------
        context: str
            Known beginning of the phrase
        continuation: str
            A likely end of the phrase

        Returns
        -------
        prob: float
            The probability
        """

        log_prob = self.single_log_proba(context, continuation)
        prob = np.exp(log_prob)
        return prob

    def generate_smart_abbreviations(self):
        """
        Generate a smart set of abbreviations

        Returns
        -------
        smart_abbrv: list[(word, abbrv)]
            A list of original word and a possible abbreviation

        Notes
        -----
        Following rules are applied in determining the possible abbreviations
            - For a word of length 'k' create abbreviations that have the first 1 to k terms
              E.g., word --> w___, wo__, wor_, word
            - Remove all vowels
              E.g., word --> wrd
            - Remove first n vowels
              E.g., template --> tmplate, tmplt, tmplte, templte, tmplat, templat, templte, etc.
            - Use stem/lemmas instead of the full word
              E.g., transaction --> transact
        """

        # lancaster_stemmer = LancasterStemmer()
        smart_abbrv = []
        uni_grams = self.corpus.split()
        counts = Counter(uni_grams)
        bi_grams = [" ".join(bg) for bg in bigrams(uni_grams)]
        words = uni_grams # + bi_grams
        for word in words:
            "Keep original word pair unchanged"
            smart_abbrv.extend(10 * [(word, word)])
            count = sum(counts[wc] for wc in word.split())
            "Keep only the first k letters"
            for i, _ in enumerate(word):
                abbrv_2 = word[:i] + (len(word) - len(word[:i])) * "_"
                abbrv_2_1 = word[:i] + "_" + word[i+1:]
                smart_abbrv.extend(30 * [(word, abbrv_2)])
                smart_abbrv.extend(30 * [(word, abbrv_2_1)])

            "Remove all vowels"
            abbrv_3 = re.sub('([aeiouyAEIOUY])', "_", word)
            smart_abbrv.extend([(word, abbrv_3)])
            "Keep only the first occurance of every letter"

            uniq = set(word)
            abbrv_5 = ""
            for letter in word:
                if letter in uniq:
                    abbrv_5+=letter
                    uniq = uniq.difference([letter])
                else:
                    abbrv_5+="_"

            vowels = re.findall('([aeiouyAEIOUy])', word)
            abbrv_5 = re.sub('([aeiouyAEIOUY])', "_", abbrv_5.lower())
            smart_abbrv.extend(30 * [(word, abbrv_5)])

            "Remove one of the vowels"
            for vowel in vowels:
                abbrv_4 = re.sub('([{}])'.format(vowel), "_", word.lower())
                smart_abbrv.extend(30 * [(word, abbrv_4)])

        return smart_abbrv


# ----------------------------------------------------------------------------------------------------

class MissingLetterModel:
    def __init__(self, order=5, smoothing_missed=0.3, smoothing_total=1.0):
        """
        This model remembers and predicts which letters are usually missed.

        Parameters
        ----------
        order: int
            Number of character the model remembers. i.e., N-1.
        smoothing_total: float
            A smoothing number added to the total counter.
        smoothing_missed: float
            A smoothing number added to the missed counter.

        Other Parameters
        ----------------
        total_counter_: int
            A counter of occurences of all letters.
        missed_counter_: int
            A counter of occurences of all the missed letters.
        """
        self.order = order
        self.smoothing_total = smoothing_total
        self.smoothing_missed = smoothing_missed
        self.total_counter_ = defaultdict(lambda: Counter())
        self.missed_counter_ = defaultdict(lambda: Counter())

    def fit(self, sentence_pairs):
        """
        Estimate the probability of missing each of the letters.

        Parameters
        ----------
        sentence_pairs: list[(str, str)]
            A list of (original_phrase, abbreviation) pairs.
        """
        for (original, observed) in sentence_pairs:
            for i, (original_letter, observed_letter) in enumerate(zip(original[self.order:], observed[self.order:])):
                context = original[i:i + self.order]
                if observed_letter == "_":
                    self.missed_counter_[context][original_letter] += 1
                self.total_counter_[context][original_letter] += 1

    def predict_proba(self, context, last_letter):
        """
        Estimate the probability of the last_letter being missed after the context.

        Parameters
        ----------
        context: str
            The word (full/partial) in context
        last_letter: str
            The letter for which we are computing the probablity

        Returns
        -------
        proba: float
            The probability value
        """

        if self.order:
            local = context[-self.order:]
        else:
            local = ""

        missed_freq = self.missed_counter_[local][last_letter] + self.smoothing_missed
        total_freq = self.total_counter_[local][last_letter] + self.smoothing_total

        return missed_freq / total_freq

    def single_log_proba(self, context, continuation, actual=None):
        """
        Compute the log probability that after the context, continuation is abbreviated to actual.
        If actual is none, then assume that nothing is abbreviated.

        Parameters
        ----------
        context: str
            Known beginning of the phrase
        continuation: str
            A likely end of the phrase
        actual: str
            The actual string

        Returns
        -------
        log_prob: float
            The log-probability
        """

        if not actual:
            actual = continuation

        log_prob = 0.0

        for orig_token, act_token in zip(continuation, actual):
            pp = self.predict_proba(context, orig_token)
            if act_token != "_":
                pp = 1 - pp
            log_prob += np.log(pp)
            context += orig_token
        return log_prob

    def single_proba(self, context, continuation, actual=None):
        """
        Compute the probability that after the context, continuation is abbreviated to actual. If
        actual is none, then assume that nothing is abbreviated.

        Parameters
        ----------
        context: str
            Known beginning of the phrase
        continuation: str
            A likely end of the phrase
        actual: str
            The actual string

        Returns
        -------
        prob: float
            The probability
        """

        log_prob = self.single_log_proba(context, continuation, actual)
        prob = np.exp(log_prob)
        return prob
