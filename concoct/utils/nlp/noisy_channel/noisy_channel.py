import numpy as np
from heapq import heappush, heappop
from ipdb import set_trace

def generate_options(prefix_proba, prefix, suffix,
                     lang_model, missed_model, optimism=0.5,
                     cache=None):
    """
    Generate partial options of abbreviations.

    Parameters
    ----------
    prefix_proba: float
        Log probability of decoded part of the abbreviation.
    prefix: str
        The decoded part of the abbreviation.
    suffix: str
        The non-decoded part of the abbreviation.
    lang_model: <utils.nlp.noisy_channel.model.LanguageNgramModel>
        The language Model.
    missed_model: <utils.nlp.noisy_channel.model.MissingLetterModel>
        The model of missing letters.
    optimism: float
        A coefficient for log likelihood of the word ends
    cache: list
        A list of suffix likelihood estimated

    Returns
    -------
    options: list[(likelihood_estimate, decoded, non-decoded, new-letter, suffix-likelihood)]
        A list of options
    """

    options = []
    for letter in lang_model.vocabulary_ + ['']:
        if letter:
            "Character missing"
            next_letter = letter
            new_suffix = suffix
            new_prefix = prefix + next_letter
            proba_missing_state = - np.log(missed_model.predict_proba(prefix, letter))
        else:
            "No missing character"
            next_letter = suffix[0]
            new_suffix = suffix[1:]
            new_prefix = prefix + next_letter
            proba_missing_state = - np.log((1 - missed_model.predict_proba(prefix, next_letter)))
        proba_next_letter = - np.log(lang_model.single_proba(prefix, next_letter))
        if cache:
            proba_suffix = cache[len(new_suffix)] * optimism
        else:
            proba_suffix = - np.log(lang_model.single_proba(new_prefix, new_suffix)) * optimism
        proba = prefix_proba + proba_next_letter + proba_missing_state + proba_suffix
        options.append((proba, new_prefix, new_suffix, letter, proba_suffix))
    return options


# ----------------------------------------------------------------------------------------------------

def decipher(abbrv, lang_model, missed_model, freedom=3.0, max_iter=10000, optimism=0.1, verbose=False):
    """
    Decipher an abbreviated word and generate possible expansions.

    Parameters
    ----------
    abbrv: str
        Abbriviated word.
    lang_model: <utils.nlp.noisy_channel.model.LanguageNgramModel>
        The language Model.
    missed_model: <utils.nlp.noisy_channel.model.MissingLetterModel>
        The model of missing letters.
    freedom: float
        The range of log-likelihood estimates.
    max_iter: int
        Upper limit of iterations.
    optimism:
        A coefficient for log likelihood of the word ends.
    verbose: Bool
        Be verbose about everything.

    Returns
    -------
    result: dict{'guessed_word': log_likelihood}
    """

    query = abbrv + ' '
    prefix = ' '
    prefix_proba = 0.0
    suffix = query
    full_origin_logprob = -lang_model.single_log_proba(prefix, query)
    no_missing_logprob = -missed_model.single_log_proba(prefix, query)
    best_logprob = full_origin_logprob + no_missing_logprob
    # add an empty prefix to the heap
    heap = [(best_logprob * optimism, prefix, suffix, '', best_logprob * optimism)]
    # add the default candidate (without missing characters)
    candidates = [(best_logprob, prefix + query, '', None, 0.0)]
    if verbose:
        print('baseline score is', best_logprob)
    # prepare storage of the phrase suffix probabilities
    cache = {}
    for i in range(len(query) + 1):
        future_suffix = query[:i]
        cache[len(future_suffix)] = -lang_model.single_log_proba('', future_suffix)  # rough approximation
        cache[len(future_suffix)] += -missed_model.single_log_proba('', future_suffix)  # at least add missingness

    for i in range(max_iter):
        if not heap:
            break
        next_best = heappop(heap)
        if verbose:
            print(next_best)
        if next_best[2] == '':  # the phrase is fully decoded
            # if the phrase is good enough, add it to the answer
            if next_best[0] <= best_logprob + freedom:
                candidates.append(next_best)
                # update estimate of the best likelihood
                if next_best[0] < best_logprob:
                    best_logprob = next_best[0]
        else:  # # the phrase is not fully decoded - generate more options
            prefix_proba = next_best[0] - next_best[4]  # all proba estimate minus suffix
            prefix = next_best[1]
            suffix = next_best[2]
            new_options = generate_options(
                prefix_proba, prefix, suffix, lang_model,
                missed_model, optimism, cache)
            # add only the solution potentioally no worse than the best + freedom
            for new_option in new_options:
                if new_option[0] < best_logprob + freedom:
                    heappush(heap, new_option)
    if verbose:
        print('heap size is', len(heap), 'after', i, 'iterations')
    result = {}
    for candidate in candidates:
        if candidate[0] <= best_logprob + freedom:
            result[candidate[1][1:-1]] = candidate[0]

    # if len(result) > 1:
    #     result.pop(abbrv)
    return result

