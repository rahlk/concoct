import os
import re
import sys
import nltk
import enchant
from time import time
from pathlib import Path
from ipdb import set_trace
from nltk.corpus import stopwords
from utils.nlp.noisy_channel.model import LanguageNgramModel, MissingLetterModel
from utils.nlp.noisy_channel.noisy_channel import decipher

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))


class Simple:
    @classmethod
    def full_word_guess(cls, abbrv):
        """
        Guess full word representation for a given abbreviation.

        Parameters
        ----------
        abbrv: str
            Abbreviated word

        Return
        ------
        guess: str
            Best guess of what the word may be

        Notes
        -----
        I am using a very simplistic approach of guessing the word from the comment string of the code.

        For a more sophisticated approach, used the Noisy Channel Estimator below.
        """
        dict_en = enchant.Dict('en_US')
        if dict_en.check(abbrv.lower()):
            return abbrv.lower()
        else:
            abbrv_len = len(abbrv)
            comment_dict = open(root.joinpath(
                "data", "dictionary", "PCCTRX.txt"))
            words = [line.rstrip("\n") for line in comment_dict.readlines()]
            guess = sorted(words, key=lambda x: nltk.edit_distance(
                abbrv.lower(), x.lower()[:abbrv_len]))[0]
            return guess


class NoisyChannelEstimation:
    def __init__(self):
        self.word_list = self._read_file(root.joinpath(
            "data", "dictionary", "business_dictionary.txt"))
        with open(root.parent.joinpath("tests/test_inputs/PCCTRX.txt"), encoding='utf-8') as f:
            text = f.read()
        text = re.sub(r'[^a-z ]+', '', text.lower().replace('\n', ' '))
        self.lang_model = LanguageNgramModel(
            order=4, smoothing=0.001, recursive=0.01)
        self.lang_model.fit(text)
        self.abbrv_model = MissingLetterModel(order=1, smoothing_missed=0.1)
        missing_set = self.lang_model.generate_smart_abbreviations()
        self.abbrv_model.fit(missing_set)

    @staticmethod
    def _read_file(file_path):
        """
        Read a file containing a list of words and return a list of words from that file

        Parameters
        ----------
        file_path: str (or) PosixPath (or) PurePath
            Path to file
        Returns
        -------
        word_list: list[str]
            A list of strings in the file read one line at a time.
        """
        with open(file_path, 'r') as f:
            word_list = [re.sub(r"\n", "", word.lower())
                         for word in f.readlines()]

        "Remove stop words (again)"
        with open(root.joinpath("data", "nlp", "stopwords.txt"), 'r') as f:
            stop_words = [re.sub(r"\n", "", word) for word in f.readlines()]

        stop_words.extend(stopwords.words('english'))

        return list(set(word_list).difference(stop_words))

    def full_word_guess(self, abbrv, opt=0.5):
        """
        Guess the full word using the noisy channel approach.

        Parameters
        ----------
        abbrv: str
            An abbreviation

        Returns
        -------
        full_word: str
            An estimate of what the full word may be.

        Notes
        -----
        For additional details, see:
            - https://goo.gl/qqpFFb
            - https://web.stanford.edu/~jurafsky/slp3/5.pdf
        """

        abbrv = re.sub(r'[^a-z ]+', ' ', abbrv.lower())
        multi_abbrv = abbrv.split()
        expansions = list()
        for single_abbrv in multi_abbrv:

            if single_abbrv in self.lang_model.corpus.split():
                deciphered = single_abbrv
            else:
                deciphered = [word for word in
                              tuple(decipher(single_abbrv, self.lang_model, self.abbrv_model, optimism=opt).keys()) if
                              word in self.lang_model.corpus.split()]
                if not len(deciphered):
                    deciphered = [single_abbrv]
            expansions.extend(deciphered)

        return tuple(expansions)
