import os
import sys
import re
from pathlib import Path
import numpy as np
from gensim.models import KeyedVectors

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))


"Setup word2vec"

"Read GLOVE text and create a word2vec file"
W2VEC_MODEL = root.joinpath("data", "nlp", "glove.model")
W2VEC_OP_FILE = root.joinpath("data", "nlp", "glove.6B.100d.txt.word2vec")

if W2VEC_MODEL.exists():
    MODEL = KeyedVectors.load(str(W2VEC_MODEL))
else:
    "Read the word2vec file to disk"
    MODEL = KeyedVectors.load_word2vec_format(W2VEC_OP_FILE, binary=False)
    MODEL.save(str(W2VEC_MODEL))


def get_word_vector(word):
    """
    Convert a word to a vector

    Parameters
    ----------
    word: str
        A word for which we need to compute a vector representation

    Return
    ------
    vect: numpy.ndarray
        A vector representation of the word

    Notes
    -----
    A few things to keep in mind here:
      + I am using a pretrained model (Standford's GloVe)
      + Obviously, it'd make sence to train our own model
    """

    filtered_word = re.sub(r"[^A-Za-z]+", '', word)
    try:
        return MODEL.wv[filtered_word.lower()]
    except:
        "**THIS IS A HACK! PLEASE FIX, PLEASE FIX**"
        return np.array(100*[0.0])
