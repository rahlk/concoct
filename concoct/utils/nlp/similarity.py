import os
import sys
import re
import enchant
import numpy as np
from pathlib import Path
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.stem.lancaster import LancasterStemmer
from scipy.spatial.distance import correlation
from pdb import set_trace
from gensim.models import Word2Vec


"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from utils.nlp.wordvec import get_word_vector, MODEL
from utils.nlp.guesstimate import Simple, NoisyChannelEstimation

def phrase_sentence_distance(phrase, sentence):
    """
    Compute distance between COBOL phrase and a sentence (usually a definiton
    from an ontology)

    Parameters
    ----------
    phrase: Str
        This is usually a cobol variable. Ex: DTRAN-CLEAN or PROCESS-TRAN

    sentence: Str
        This is the definition we obtain from our ontology.

    Return
    ------
    dist: float
        The mean cosine distance between all the words in the phrase (if the phrase contains abbreviations, the
        we expand them) and the sentence definition of the ontological concept.

    Notes
    -----
    There are a lot of possibilities here on how we map the variables to full sentences. Ex: LDA or LSI based topic
    modeling, or using doc2vec and word2vec together, etc.
    """

    dist = 1e32
    phraselets = phrase.split()  # split words in the phrase
    sentence = re.sub(r'[^a-z ]+', ' ', sentence.lower())  # keep only words
    stop_words = set(stopwords.words('english'))  # Get common english stop words
    sentence_tokens = sentence.split()  # get tokens from the sentence
    sentence_tokens = list(set(sentence_tokens).difference(stop_words))  # remove stop words

    "Initialize NLTK Stuff"
    lemmatizer = WordNetLemmatizer()
    stemmer = LancasterStemmer()
    dict_en = enchant.Dict('en_US')

    "Other initializations"
    noisy_est = NoisyChannelEstimation()

    cdist_mtx = []
    for i, word in enumerate(phraselets):
        full_word = Simple.full_word_guess(word)
        # full_word = noisy_est.full_word_guess(word)
        if isinstance(full_word, tuple):
            full_word = full_word[0]

        full_word_stemmed = stemmer.stem(full_word)
        full_word_lemmaed = lemmatizer.lemmatize(full_word)
        chosen_word = full_word_stemmed if dict_en.check(full_word_stemmed) else full_word_lemmaed
        full_word_vec = get_word_vector(chosen_word)
        for j, sent in enumerate(sentence_tokens):
            sent_stemmed = stemmer.stem(sent)
            sent_lemmaed = lemmatizer.lemmatize(sent)
            chosen_sent = sent_stemmed if dict_en.check(sent_stemmed) else sent_lemmaed
            sent_word_vec = get_word_vector(chosen_sent)
            cdist_mtx.append(round(correlation(full_word_vec, sent_word_vec), 2))

    n = len(cdist_mtx)
    # sorted_cdist = sorted(cdist_mtx)
    try:
        dist = np.min(cdist_mtx)
    except ValueError:
        return 1
    # dist = np.mean(sorted_cdist[:int(0.1*n)])

    return dist
