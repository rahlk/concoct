import re
from ipdb import set_trace
from nltk.corpus import stopwords


class DictionaryParse:
    def __init__(self, pdf_file):
        """
        This class read a pdf document and parses the text within it
        Parameters
        ----------
        pdf_file
        """
        self.pdf_file = pdf_file

    def extract_tokens(self):
        """
        Extract text from a given PDF file

        Returns
        -------
        text: str
            A string comprising of text in the pdf
        """
        with open(self.pdf_file, "r") as doc:
            for line in doc.readlines():
                if line.__contains__(":"):
                    word = line.split(":")[0]
                    print(word)
                    set_trace()