"""
This program provides all the necessary I/O functionalities for reading/writing RDF files.
"""

from __future__ import print_function
import os
import re
import json
import shlex
import rdflib as rdf
import subprocess as subproc
from rdflib.extras.external_graph_libs import rdflib_to_networkx_multidigraph

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


class OntoIO:
    def __init__(self, filename):
        self.filename = filename

    def read_from_json(self):
        """
        Read an OWL file (saved using Protege in this case)

        Parameters
        ----------
        filename: Str
            Location of the owl file, formatted as a string. E.g. "path/to/owl/file"

        Return
        ------
        onto: dict
            Ontology File as a dictionary
        """
        file = json.load(open(self.filename, "r"))
        return file

    def read(self):
        """
        Read an ontology file (of type: rdf, json, ttl)

        Parameters
        ----------
        filename: String
            Full path of the file

        Return
        ------
        g: <rdflib.graph.Graph>
            Ontology graph
        """
        filename = os.path.abspath(self.filename)
        g = rdf.Graph()
        g.parse(filename)
        return g

    @staticmethod
    def rdflib_graph_to_networkx(graph):
        """
        Convert an ontology graph in rdflib format to NetworkX MultiDiGraph

        Parameters
        ----------
        graph: <rdflib.graph.Graph>
            Ontology graph in rdflib format

        Return
        ------
        g: <nx.classes.multidigraph.MultiDiGraph>
            A NeworkX MultiDiGraph
        """
        g = rdflib_to_networkx_multidigraph(graph)
        return g


    def write_as_image(self, savename=None):
        """
        Save a given ontology graph as an image

        Parameters
        ----------
        filename: String
            Full path of the file
        savename: String
            Full path including name of the file to be saved. Note: Optional.
        savetype: String
            Save type. Defaults to dot, also try: pdf, png, ps, eps, ...

        Return
        ------
        N/A:
            No returns
        """

        if savename is None:
            savename = re.sub(".rdf", ".dot", str(self.filename))
        cmd = shlex.split("rdf2dot {}".format(str(self.filename)))
        p = subproc.Popen(cmd, stdout=subproc.PIPE, stderr=subproc.PIPE)
        print(p.stdout.read(), file=open(savename, "w+"))

    @classmethod
    def write_to_file(cls, graph, savename, savetype='turtle'):
        """
        Write an ontology graph to file

        Parameters
        ----------
        g: <rdflib.graph.Graph>
            Ontology graph
        savename: String
            Full path including name of the file to be saved. Note: Optional.
        savetype: String
            Save type. Defaults to dot, also try: pdf, png, ps, eps, ...

        Return
        ------
        N/A:
            No returns
        """
        return graph.serialize(destination=savename, format=savetype)

    def get_definitions_and_labels(self):
        """
        Parse a SPARQL query to extract labels and definitions from a given ontology

        Parameters
        ----------
        N/A: None
            This method takes no input paramters

        Returns
        -------
        onto_dict: dict
            A dictionary of <label,definition> pairs

        Notes
        -----

        I use the following SPARQL query:

            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            SELECT ?type ?define
            WHERE
            {
                ?class rdfs:label ?type .
                ?class skos:definition ?define .
            }
        """

        onto_graph = self.read()
        qres = onto_graph.query(
            """
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            SELECT ?type ?define
            WHERE 
            {
                ?class rdfs:label ?type . 
                ?class skos:definition ?define .
            }
            """
        )
        onto_dict = dict()
        for row in qres:
            onto_dict.update({"{}".format(row[0]): "{}".format(row[1])})
        return onto_dict
