      *-----------------------------------------------------------
      *
      * PCCTRE - Phase Change Credit Daily Transaction Edit
      *
      * Program to edit daily credit card transactions.
      * Valid transactions are written to a file for further
      * processing and invalid transactions are written to a
      * file to be corrected.
      * A brief report is also written.
      *
      * Alphabetical list of COBOL verbs used:
      *  ACCEPT, ADD, CLOSE, CONTINUE, DISPLAY, EXIT, IF
      *  MOVE, OPEN, PERFORM, READ, SET, STOP, STRING, WRITE
      *
      * Input: TRAND - Input Daily Transactions
      *
      * Output: TRANC - Output cleaned/verified transactions
      *         TRANE - Output exception transactions
      *         REPT  - Output report
      *
      *-----------------------------------------------------------
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  PCCTRE.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
        SELECT DAILY-TRANSACTIONS
               ASSIGN       TO TRAND
               ORGANIZATION IS SEQUENTIAL
               ACCESS MODE  IS SEQUENTIAL
               FILE STATUS  IS TRAND-STATUS.
        SELECT CLEANED-TRANSACTIONS
               ASSIGN       TO TRANC
               ORGANIZATION IS SEQUENTIAL
               ACCESS MODE  IS SEQUENTIAL
               FILE STATUS  IS TRANC-STATUS.
        SELECT EXCEPTION-TRANSACTIONS
               ASSIGN       TO TRANE
               ORGANIZATION IS SEQUENTIAL
               ACCESS MODE  IS SEQUENTIAL
               FILE STATUS  IS TRANE-STATUS.
        SELECT REPT
               ASSIGN       TO REPT
               ORGANIZATION IS SEQUENTIAL
               ACCESS MODE  IS SEQUENTIAL
               FILE STATUS  IS REPORT-STATUS.
      *
       DATA DIVISION.
       FILE SECTION.
       FD  DAILY-TRANSACTIONS
           RECORD CONTAINS 57 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  TRAND-RECORD           PIC X(57).
      *
       FD  CLEANED-TRANSACTIONS
           RECORD CONTAINS 57 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  TRANC-RECORD           PIC X(57).
      *
       FD  EXCEPTION-TRANSACTIONS
           RECORD CONTAINS 58 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  TRANE-RECORD           PIC X(58).
      *
       FD  REPT
           RECORD CONTAINS 70 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  REPORT-RECORD          PIC X(70).
      *
       WORKING-STORAGE SECTION.
       01  WS-AREA.
           05  WS-MSG             PIC X(70).
           05  WS-DATE.
               10  WS-DATE-YY     PIC 9(4).
               10  WS-DATE-MM     PIC 99.
               10  WS-DATE-DD     PIC 99.
           05  WS-TIME.
               10  WS-TIME-HH     PIC 99.
               10  WS-TIME-MM     PIC 99.
               10  WS-TIME-SS     PIC 99.
               10  WS-TIME-HT     PIC 99.
           05  TRAND-ENDFILE      PIC X.
               88 TRAND-AT-ENDFILE      VALUE 'Y'.
               88 TRAND-NOT-AT-ENDFILE  VALUE 'N'.
           05  EDIT-FLAG          PIC X.
               88 EDIT-SUCCEDED   VALUE 'Y'.
               88 EDIT-FAILED     VALUE 'N'.
           05  TRAND-REC-COUNT    PIC 9(4) COMP VALUE 0.
           05  TRANC-REC-COUNT    PIC 9(4) COMP VALUE 0.
           05  TRANE-REC-COUNT    PIC 9(4) COMP VALUE 0.
           05  TRAND-REC-COUNT-D  PIC 9(4).
           05  TRANC-REC-COUNT-D  PIC 9(4).
           05  TRANE-REC-COUNT-D  PIC 9(4).
       01  FILE-STATUS.
           05  TRAND-STATUS.
               10  TRAND-STATUS-1      PIC X.
                   88  TRAND-OK   VALUE '0'.
                   88  TRAND-EOF  VALUE '1'.
               10  TRAND-STATUS-2       PIC X.
           05  TRANC-STATUS.
               10  TRANC-STATUS-1      PIC X.
                   88  TRANC-OK   VALUE '0'.
                   88  TRANC-EOF  VALUE '1'.
               10  TRANC-STATUS-2       PIC X.
           05  TRANE-STATUS.
               10  TRANE-STATUS-1      PIC X.
                   88  TRANE-OK   VALUE '0'.
                   88  TRANE-EOF  VALUE '1'.
               10  TRANE-STATUS-2       PIC X.
           05  REPORT-STATUS.
               10  REPORT-STATUS-1     PIC X.
                   88  REPORT-OK  VALUE '0'.
                   88  REPORT-EOF VALUE '1'.
               10  REPORT-STATUS-2      PIC X.
      *
          COPY DTRANS.
      *
          COPY ETRANS.
      *
       PROCEDURE DIVISION.
      *
           PERFORM OPEN-FILES.
           PERFORM GET-DATE-TIME.
      *
           SET TRAND-NOT-AT-ENDFILE TO TRUE.
           PERFORM PROCESS-TRANSACTIONS UNTIL TRAND-AT-ENDFILE.
      *
           MOVE SPACES TO WS-MSG.
           STRING 'PCCTRE COMPLETED NORMALLY - Processed '
                  TRAND-REC-COUNT-D ' Daily Transactions.'
                  DELIMITED BY SIZE INTO WS-MSG
           END-STRING.
           DISPLAY WS-MSG.
           WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 2 LINES.
           MOVE SPACES TO WS-MSG.
           STRING 'Wrote ' TRANC-REC-COUNT-D ' Clean Transactions and '
                  TRANE-REC-COUNT-D ' Exception Transactions.'
                  DELIMITED BY SIZE INTO WS-MSG
           END-STRING.
           DISPLAY WS-MSG.
           WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 2 LINES.
      *
           PERFORM CLOSE-FILES.
      *
           STOP RUN.
      *
       PROCESS-TRANSACTIONS.
           READ DAILY-TRANSACTIONS INTO DAILY-TRANSACTION-RECORD
               AT END SET TRAND-AT-ENDFILE TO TRUE
               NOT AT END PERFORM EDIT-TRANSACTION
           END-READ.
           EXIT.
      *
       EDIT-TRANSACTION.
           ADD 1 TO TRAND-REC-COUNT
               GIVING TRAND-REC-COUNT TRAND-REC-COUNT-D
           SET EDIT-SUCCEDED TO TRUE.
           PERFORM EDIT-TRANSACTION-FIELDS.
           IF EDIT-FAILED THEN
               MOVE DAILY-TRANSACTION-RECORD TO
                   ETRAN-ORIGINAL-RECORD
               WRITE TRANE-RECORD FROM EXCEPTION-TRANSACTION-RECORD
               ADD 1 TO TRANE-REC-COUNT
                   GIVING TRANE-REC-COUNT TRANE-REC-COUNT-D
           ELSE
               WRITE TRANC-RECORD FROM DAILY-TRANSACTION-RECORD
               ADD 1 TO TRANC-REC-COUNT
                   GIVING TRANC-REC-COUNT TRANC-REC-COUNT-D
           END-IF.
      *
       EDIT-TRANSACTION-FIELDS.
      *
      * Edit Account Number
      *
           IF DTRAN-ACCOUNT-NUMBER IS NOT NUMERIC THEN
               SET EDIT-FAILED ETRAN-INVALID-ACCT TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit transaction date
      *
           IF DTRAN-DATE-YYYYMMDD IS NOT NUMERIC THEN
               SET EDIT-FAILED ETRAN-INVALID-DATE TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit transaction time
      *
           IF DTRAN-TIME-HHMMSS IS NOT NUMERIC THEN
               SET EDIT-FAILED ETRAN-INVALID-TIME TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit transaction amount
      *
           IF DTRAN-AMOUNT IS NOT NUMERIC THEN
               SET EDIT-FAILED ETRAN-INVALID-AMOUNT TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit transaction type (debit/credit)
      *
           IF DTRAN-DEBIT OR DTRAN-CREDIT THEN
               CONTINUE
           ELSE
               SET EDIT-FAILED ETRAN-INVALID-TYPE TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit currency code - USD/CAD/EUR/GBP
      * Default to 'USD' if blank
      *
           IF DTRAN-CURRENCY-CODE =
               'USD' OR 'CAD' OR 'EUR' OR 'GBP' THEN
               CONTINUE
           ELSE
               IF DTRAN-CURRENCY-CODE = SPACES THEN
                   MOVE 'USD' TO DTRAN-CURRENCY-CODE
               ELSE
                   SET EDIT-FAILED ETRAN-INVALID-CURRENCY TO TRUE
                   EXIT PARAGRAPH
               END-IF
           END-IF.
      *
      * Edit retailer code
      *
           IF DTRAN-RETAILER-CODE IS NOT NUMERIC THEN
               SET EDIT-FAILED ETRAN-INVALID-RETAILER TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit authorization code
      *
           IF DTRAN-AUTH-CODE IS NOT NUMERIC THEN
               SET EDIT-FAILED ETRAN-INVALID-AUTH-CODE TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
      * Edit authorization code
      *
           IF DTRAN-PAPER-ENTRY OR
               DTRAN-MANUAL-ENTER OR
               DTRAN-CARD-SWIPED OR
               DTRAN-CHIP-READ
               CONTINUE
           ELSE
               SET EDIT-FAILED ETRAN-INVALID-CODE TO TRUE
               EXIT PARAGRAPH
           END-IF.
      *
           EXIT.
      *
       OPEN-FILES.
           OPEN INPUT DAILY-TRANSACTIONS.
           IF NOT TRAND-OK
               STRING 'TRAND OPEN Error - File Status is: '
                      TRAND-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           OPEN OUTPUT CLEANED-TRANSACTIONS.
           IF NOT TRANC-OK
               STRING 'TRANC OPEN Error - File Status is: '
                      TRANC-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           OPEN OUTPUT EXCEPTION-TRANSACTIONS.
           IF NOT TRANE-OK
               STRING 'TRANE OPEN Error - File Status is: '
                      TRANE-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           OPEN OUTPUT REPT.
           IF NOT REPORT-OK
               STRING 'REPORT OPEN Error - File Status is: '
                      REPORT-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           EXIT.
      *
       CLOSE-FILES.
           CLOSE DAILY-TRANSACTIONS.
           CLOSE CLEANED-TRANSACTIONS.
           CLOSE EXCEPTION-TRANSACTIONS.
           CLOSE REPT.
           EXIT.
      *
       GET-DATE-TIME.
           ACCEPT WS-DATE FROM DATE YYYYMMDD.
           ACCEPT WS-TIME FROM TIME.
           MOVE SPACES TO WS-MSG.
           STRING 'PCCTRE Started on '
                  WS-DATE-YY '/' WS-DATE-MM '/' WS-DATE-DD ' at '
                  WS-TIME-HH ':' WS-TIME-MM ':' WS-TIME-SS
                  DELIMITED BY SIZE INTO WS-MSG
           END-STRING.
           DISPLAY WS-MSG.
           WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 1 LINE.
           EXIT.
