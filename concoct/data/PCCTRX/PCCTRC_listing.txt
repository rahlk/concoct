1PP 5655-W32 IBM Enterprise COBOL for z/OS  5.2.0                         Date 06/05/2018  Time 14:55:07   Page     1
0Invocation parameters:
  ADATA,MAP,LINECOUNT(0),VBREF,XREF(FULL)
0Options in effect:
    ADATA
    ADV
    AFP(VOLATILE)
    QUOTE
    ARCH(7)
    ARITH(COMPAT)
  NOAWO
  NOBLOCK0
    BUFSIZE(4096)
  NOCICS
    CODEPAGE(1140)
  NOCOMPILE(S)
  NOCOPYRIGHT
  NOCURRENCY
    DATA(31)
    DBCS
  NODECK
  NODIAGTRUNC
    DISPSIGN(COMPAT)
  NODLL
  NODUMP
  NODYNAM
  NOEXIT
  NOEXPORTALL
  NOFASTSRT
    FLAG(I,I)
  NOFLAGSTD
    HGPR(PRESERVE)
  NOINITCHECK
    INTDATE(ANSI)
    LANGUAGE(EN)
    LINECOUNT(0)
  NOLIST
    MAP(HEX)
    MAXPCF(60000)
  NOMDECK
  NONAME
    NSYMBOL(NATIONAL)
  NONUMBER
  NONUMCHECK
    NUMPROC(NOPFD)
    OBJECT
  NOOFFSET
    OPTIMIZE(0)
    OUTDD(SYSOUT)
    PGMNAME(COMPAT)
    QUALIFY(COMPAT)
    RENT
    RMODE(AUTO)
  NORULES
  NOSERVICE
    SEQUENCE
    SOURCE
    SPACE(1)
  NOSQL
    SQLCCSID
  NOSQLIMS
  NOSSRANGE
  NOSTGOPT
  NOTERM
  NOTEST(NODWARF)
  NOTHREAD
    TRUNC(STD)
    VBREF
    VLR(STANDARD)
    VSAMOPENFS(COMPAT)
  NOWORD
    XMLPARSE(XMLSS)
    XREF(FULL)
    ZONEDATA(PFD)
    ZWB
   LineID  PL SL  ----+-*A-1-B--+----2----+----3----+----4----+----5----+----6----+----7-|--+----8 Map and Cross Reference
   000001               *-----------------------------------------------------------
   000002               *
   000003               * PCCTRC - Phase Change Credit Daily Transaction Edit
   000004               *
   000005               * Program to convert currency to USD in all daily
   000006               * credit card transactions.
   000007               * Converted transactions are written to a file for further
   000008               * processing.
   000009               * A brief report is also written.
   000010               *
   000011               * Alphabetical list of COBOL verbs used:
   000012               * ACCEPT, ADD, CLOSE, COMPUTE, CONTINUE, DISPLAY, EXIT, IF, MOVE,
   000013               * OPEN, PERFORM, READ, SET, STOP, STRING, WRITE
   000014               *
   000015               * Input: TRANC - Input cleaned/verified Transactions
   000016               *
   000017               * Output: TRANU - Output transacions converted to USD
   000018               *         REPT  - Output report
   000019               *
   000020               *-----------------------------------------------------------
   000021                IDENTIFICATION DIVISION.
   000022                PROGRAM-ID.  PCCTRC.
   000023                ENVIRONMENT DIVISION.
   000024                INPUT-OUTPUT SECTION.
   000025                FILE-CONTROL.
   000026                    SELECT CLEANED-TRANSACTIONS                                           44
   000027                           ASSIGN       TO TRANC
   000028                           ORGANIZATION IS SEQUENTIAL
   000029                           ACCESS MODE  IS SEQUENTIAL
   000030                           FILE STATUS  IS TRANC-STATUS.                                  82
   000031                    SELECT CONVERTED-TRANSACTIONS                                         50
   000032                           ASSIGN       TO TRANU
   000033                           ORGANIZATION IS SEQUENTIAL
   000034                           ACCESS MODE  IS SEQUENTIAL
   000035                           FILE STATUS  IS TRANU-STATUS.                                  87
   000036                    SELECT REPT                                                           56
   000037                           ASSIGN       TO REPT
   000038                           ORGANIZATION IS SEQUENTIAL
   000039                           ACCESS MODE  IS SEQUENTIAL
   000040                           FILE STATUS  IS REPORT-STATUS.                                 92
   000041               *
   000042                DATA DIVISION.
   000043                FILE SECTION.
   000044                FD  CLEANED-TRANSACTIONS
   000045                    RECORD CONTAINS 57 CHARACTERS
   000046                    BLOCK CONTAINS 0 CHARACTERS
   000047                    RECORDING MODE IS F.
   000048                01  TRANC-RECORD           PIC X(57).                                     BLF=00001,000000000 57C
   000049               *
   000050                FD  CONVERTED-TRANSACTIONS
   000051                    RECORD CONTAINS 57 CHARACTERS
   000052                    BLOCK CONTAINS 0 CHARACTERS
   000053                    RECORDING MODE IS F.
   000054                01  TRANU-RECORD           PIC X(57).                                     BLF=00002,000000000 57C
   000055               *
   000056                FD  REPT
   000057                    RECORD CONTAINS 70 CHARACTERS
   000058                    BLOCK CONTAINS 0 CHARACTERS
   000059                    RECORDING MODE IS F.
   000060                01  REPORT-RECORD          PIC X(70).                                     BLF=00003,000000000 70C
   000061               *
   000062                WORKING-STORAGE SECTION.
   000063                01  WS-AREA.                                                                        000000000 0CL99
   000064                    05  WS-MSG             PIC X(70).                                               000000000 70C
   000065                    05  WS-DATE.                                                                    000000046 0CL8
   000066                        10  WS-DATE-YY     PIC 9(4).                                                000000046 4C
   000067                        10  WS-DATE-MM     PIC 99.                                                  00000004A 2C
   000068                        10  WS-DATE-DD     PIC 99.                                                  00000004C 2C
   000069                    05  WS-TIME.                                                                    00000004E 0CL8
   000070                        10  WS-TIME-HH     PIC 99.                                                  00000004E 2C
   000071                        10  WS-TIME-MM     PIC 99.                                                  000000050 2C
   000072                        10  WS-TIME-SS     PIC 99.                                                  000000052 2C
   000073                        10  WS-TIME-HT     PIC 99.                                                  000000054 2C
   000074                    05  TRANC-ENDFILE      PIC X.                                                   000000056 1C
   000075                        88 TRANC-AT-ENDFILE      VALUE 'Y'.
   000076                        88 TRANC-NOT-AT-ENDFILE  VALUE 'N'.
   000077                    05  TRANC-REC-COUNT    PIC 9(4) COMP VALUE 0.                                   000000057 2C
   000078                    05  TRANU-REC-COUNT    PIC 9(4) COMP VALUE 0.                                   000000059 2C
   000079                    05  TRANC-REC-COUNT-D  PIC 9(4).                                                00000005B 4C
   000080                    05  TRANU-REC-COUNT-D  PIC 9(4).                                                00000005F 4C
   000081                01  FILE-STATUS.                                                                    000000000 0CL6
   000082                    05  TRANC-STATUS.                                                               000000000 0CL2
   000083                        10  TRANC-STATUS-1      PIC X.                                              000000000 1C
   000084                            88  TRANC-OK   VALUE '0'.
   000085                            88  TRANC-EOF  VALUE '1'.
   000086                        10  TRANC-STATUS-2       PIC X.                                             000000001 1C
   000087                    05  TRANU-STATUS.                                                               000000002 0CL2
   000088                        10  TRANU-STATUS-1      PIC X.                                              000000002 1C
   000089                            88  TRANU-OK   VALUE '0'.
   000090                            88  TRANU-EOF  VALUE '1'.
   000091                        10  TRANU-STATUS-2       PIC X.                                             000000003 1C
   000092                    05  REPORT-STATUS.                                                              000000004 0CL2
   000093                        10  REPORT-STATUS-1     PIC X.                                              000000004 1C
   000094                            88  REPORT-OK  VALUE '0'.
   000095                            88  REPORT-EOF VALUE '1'.
   000096                        10  REPORT-STATUS-2      PIC X.                                             000000005 1C
   000097               *
   000098                   COPY DTRANS.
   000099C              *
   000100C              * PCC (Phase Change Credit) Daily Transaction File - 57 bytes
   000101C              *
   000102C               01  DAILY-TRANSACTION-RECORD.                                                       000000000 0CL57
   000103C                   05  DTRAN-ACCOUNT-NUMBER        PIC 9(16).                                      000000000 16C
   000104C                   05  DTRAN-DATE-YYYYMMDD         PIC 9(8).                                       000000010 8C
   000105C                   05  DTRAN-TIME-HHMMSS           PIC 9(6).                                       000000018 6C
   000106C                   05  DTRAN-AMOUNT                PIC 9(8)V99.                                    00000001E 10C
   000107C                   05  DTRAN-TYPE                  PIC X.                                          000000028 1C
   000108C                       88 DTRAN-DEBIT              VALUE 'D'.
   000109C                       88 DTRAN-CREDIT             VALUE 'C'.
   000110C                   05  DTRAN-CURRENCY-CODE         PIC XXX.                                        000000029 3C
   000111C                   05  DTRAN-RETAILER-CODE         PIC 9(6).                                       00000002C 6C
   000112C                   05  DTRAN-AUTH-CODE             PIC 9(6).                                       000000032 6C
   000113C                   05  DTRAN-CODE                  PIC X.                                          000000038 1C
   000114C                       88 DTRAN-PAPER-ENTRY        VALUE 'P'.
   000115C                       88 DTRAN-MANUAL-ENTER       VALUE 'M'.
   000116C                       88 DTRAN-CARD-SWIPED        VALUE 'S'.
   000117C                       88 DTRAN-CHIP-READ          VALUE 'C'.
   000118               *
   000119                PROCEDURE DIVISION.
   000120               *
   000121                    PERFORM OPEN-FILES.                                                   185
   000122                    PERFORM GET-DATE-TIME.                                                218
   000123               *
   000124                    SET TRANC-NOT-AT-ENDFILE TO TRUE.                                     76
   000125                    PERFORM PROCESS-TRANSACTIONS UNTIL TRANC-AT-ENDFILE.                  143 75
   000126               *
   000127                    MOVE 'PCCTRC COMPLETED NORMALLY' TO WS-MSG.                           64
   000128                    DISPLAY WS-MSG.                                                       64
   000129                    WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 2 LINES.              60 64
   000130                    MOVE SPACES TO WS-MSG.                                                IMP 64
   000131                    STRING 'Processed '
   000132                           TRANC-REC-COUNT-D ' Daily Transactions In and '                79
   000133                           TRANU-REC-COUNT-D ' Daily Transactions Out.'                   80
   000134                           DELIMITED BY SIZE INTO WS-MSG                                  64
   000135                    END-STRING.
   000136                    DISPLAY WS-MSG.                                                       64
   000137                    WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 2 LINES.              60 64
   000138               *
   000139                    PERFORM CLOSE-FILES.                                                  212
   000140               *
   000141                    STOP RUN.
   000142               *
   000143                PROCESS-TRANSACTIONS.
   000144                    READ CLEANED-TRANSACTIONS INTO DAILY-TRANSACTION-RECORD               44 102
   000145      1                 AT END SET TRANC-AT-ENDFILE TO TRUE                               75
   000146      1                 NOT AT END PERFORM CONVERT-TRANSACTION                            150
   000147                    END-READ.
   000148                    EXIT.
   000149               *
   000150                CONVERT-TRANSACTION.
   000151                    ADD 1 TO TRANC-REC-COUNT                                              77
   000152                        GIVING TRANC-REC-COUNT TRANC-REC-COUNT-D.                         77 79
   000153
   000154                    IF DTRAN-CURRENCY-CODE = 'USD' THEN                                   110
   000155      1                 CONTINUE
   000156                    ELSE
   000157      1                 IF DTRAN-CURRENCY-CODE = 'CAD' THEN                               110
   000158      2                     PERFORM CONVERT-CANADIAN                                      176
   000159      1                 ELSE
   000160      2                     IF DTRAN-CURRENCY-CODE = 'EUR' THEN                           110
   000161      3                         PERFORM CONVERT-EUROS                                     179
   000162      2                     ELSE
   000163      3                         IF DTRAN-CURRENCY-CODE = 'GBP' THEN                       110
   000164      4                             PERFORM CONVERT-POUNDS                                182
   000165      3                         END-IF
   000166      2                     END-IF
   000167      1                 END-IF
   000168                    END-IF.
   000169
   000170                    MOVE 'USD' TO DTRAN-CURRENCY-CODE.                                    110
   000171                    WRITE TRANU-RECORD FROM DAILY-TRANSACTION-RECORD.                     54 102
   000172                    ADD 1 TO TRANU-REC-COUNT                                              78
   000173                        GIVING TRANU-REC-COUNT TRANU-REC-COUNT-D.                         78 80
   000174                    EXIT.
   000175               *
   000176                CONVERT-CANADIAN.
   000177                    COMPUTE DTRAN-AMOUNT ROUNDED = DTRAN-AMOUNT * 1.29514.                106 106
   000178               *
   000179                CONVERT-EUROS.
   000180                    COMPUTE DTRAN-AMOUNT ROUNDED = DTRAN-AMOUNT * 0.85760.                106 106
   000181               *
   000182                CONVERT-POUNDS.
   000183                    COMPUTE DTRAN-AMOUNT ROUNDED = DTRAN-AMOUNT * 0.74929.                106 106
   000184               *
   000185                OPEN-FILES.
   000186                    OPEN INPUT CLEANED-TRANSACTIONS.                                      44
   000187                    IF NOT TRANC-OK                                                       84
   000188      1                 STRING 'TRANC OPEN Error - File Status is: '
   000189      1                        TRANC-STATUS DELIMITED BY SIZE INTO WS-MSG                 82 64
   000190      1                 END-STRING
   000191      1                 DISPLAY WS-MSG                                                    64
   000192      1                 STOP RUN.
   000193               *
   000194                    OPEN OUTPUT CONVERTED-TRANSACTIONS.                                   50
   000195                    IF NOT TRANU-OK                                                       89
   000196      1                 STRING 'TRANU OPEN Error - File Status is: '
   000197      1                        TRANU-STATUS DELIMITED BY SIZE INTO WS-MSG                 87 64
   000198      1                 END-STRING
   000199      1                 DISPLAY WS-MSG                                                    64
   000200      1                 STOP RUN.
   000201               *
   000202                    OPEN OUTPUT REPT.                                                     56
   000203                    IF NOT REPORT-OK                                                      94
   000204      1                 STRING 'REPORT OPEN Error - File Status is: '
   000205      1                        REPORT-STATUS DELIMITED BY SIZE INTO WS-MSG                92 64
   000206      1                 END-STRING
   000207      1                 DISPLAY WS-MSG                                                    64
   000208      1                 STOP RUN.
   000209               *
   000210                    EXIT.
   000211               *
   000212                CLOSE-FILES.
   000213                    CLOSE CLEANED-TRANSACTIONS.                                           44
   000214                    CLOSE CONVERTED-TRANSACTIONS.                                         50
   000215                    CLOSE REPT.                                                           56
   000216                    EXIT.
   000217               *
   000218                GET-DATE-TIME.
   000219                    ACCEPT WS-DATE FROM DATE YYYYMMDD.                                    65 65
   000220                    ACCEPT WS-TIME FROM TIME.                                             69
   000221                    MOVE SPACES TO WS-MSG.                                                IMP 64
   000222                    STRING 'PCCTRC Started on '
   000223                           WS-DATE-YY '/' WS-DATE-MM '/' WS-DATE-DD ' at '                66 67 68
   000224                           WS-TIME-HH ':' WS-TIME-MM ':' WS-TIME-SS                       70 71 72
   000225                           DELIMITED BY SIZE INTO WS-MSG                                  64
   000226                    END-STRING.
   000227                    DISPLAY WS-MSG.                                                       64
   000228                    WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 1 LINE.               60 64
   000229                    EXIT.
0 Count   Cross-reference of verbs        References

 2        ACCEPT . . . . . . . . . . . .  219 220
 2        ADD. . . . . . . . . . . . . .  151 172
 3        CLOSE. . . . . . . . . . . . .  213 214 215
 3        COMPUTE. . . . . . . . . . . .  177 180 183
 1        CONTINUE . . . . . . . . . . .  155
 6        DISPLAY. . . . . . . . . . . .  128 136 191 199 207 227
 5        EXIT . . . . . . . . . . . . .  148 174 210 216 229
 7        IF . . . . . . . . . . . . . .  154 157 160 163 187 195 203
 4        MOVE . . . . . . . . . . . . .  127 130 170 221
 3        OPEN . . . . . . . . . . . . .  186 194 202
 8        PERFORM. . . . . . . . . . . .  121 122 125 139 146 158 161 164
 1        READ . . . . . . . . . . . . .  144
 2        SET. . . . . . . . . . . . . .  124 145
 4        STOP . . . . . . . . . . . . .  141 192 200 208
 5        STRING . . . . . . . . . . . .  131 188 196 204 222
 4        WRITE. . . . . . . . . . . . .  129 137 171 228
 An "M" preceding a data-name reference indicates that the data-name is modified by this reference.

  Defined   Cross-reference of data names   References

       44   CLEANED-TRANSACTIONS . . . . .  26 144 186 213
       50   CONVERTED-TRANSACTIONS . . . .  31 194 214
      102   DAILY-TRANSACTION-RECORD . . .  M144 171
      103   DTRAN-ACCOUNT-NUMBER
      106   DTRAN-AMOUNT . . . . . . . . .  M177 177 M180 180 M183 183
      112   DTRAN-AUTH-CODE
      116   DTRAN-CARD-SWIPED
      117   DTRAN-CHIP-READ
      113   DTRAN-CODE
      109   DTRAN-CREDIT
      110   DTRAN-CURRENCY-CODE. . . . . .  154 157 160 163 M170
      104   DTRAN-DATE-YYYYMMDD
      108   DTRAN-DEBIT
      115   DTRAN-MANUAL-ENTER
      114   DTRAN-PAPER-ENTRY
      111   DTRAN-RETAILER-CODE
      105   DTRAN-TIME-HHMMSS
      107   DTRAN-TYPE
       81   FILE-STATUS
       95   REPORT-EOF
       94   REPORT-OK. . . . . . . . . . .  203
       60   REPORT-RECORD. . . . . . . . .  M129 M137 M228
       92   REPORT-STATUS. . . . . . . . .  40 205
       93   REPORT-STATUS-1
       96   REPORT-STATUS-2
       56   REPT . . . . . . . . . . . . .  36 202 215
       75   TRANC-AT-ENDFILE . . . . . . .  125 M145
       74   TRANC-ENDFILE
       85   TRANC-EOF
       76   TRANC-NOT-AT-ENDFILE . . . . .  M124
       84   TRANC-OK . . . . . . . . . . .  187
       77   TRANC-REC-COUNT. . . . . . . .  151 M152
       79   TRANC-REC-COUNT-D. . . . . . .  132 M152
       48   TRANC-RECORD
       82   TRANC-STATUS . . . . . . . . .  30 189
       83   TRANC-STATUS-1
       86   TRANC-STATUS-2
       90   TRANU-EOF
       89   TRANU-OK . . . . . . . . . . .  195
       78   TRANU-REC-COUNT. . . . . . . .  172 M173
       80   TRANU-REC-COUNT-D. . . . . . .  133 M173
       54   TRANU-RECORD . . . . . . . . .  M171
       87   TRANU-STATUS . . . . . . . . .  35 197
       88   TRANU-STATUS-1
       91   TRANU-STATUS-2
       63   WS-AREA
       65   WS-DATE. . . . . . . . . . . .  M219 219
       68   WS-DATE-DD . . . . . . . . . .  223
       67   WS-DATE-MM . . . . . . . . . .  223
       66   WS-DATE-YY . . . . . . . . . .  223
       64   WS-MSG . . . . . . . . . . . .  M127 128 129 M130 M134 136 137 M189 191 M197 199 M205 207 M221 M225 227 228
       69   WS-TIME. . . . . . . . . . . .  M220
       70   WS-TIME-HH . . . . . . . . . .  224
       73   WS-TIME-HT
       71   WS-TIME-MM . . . . . . . . . .  224
       72   WS-TIME-SS . . . . . . . . . .  224
 Context usage is indicated by the letter preceding a procedure-name reference.
 These letters and their meanings are:
     A = ALTER (procedure-name)
     D = GO TO (procedure-name) DEPENDING ON
     E = End of range of (PERFORM) through (procedure-name)
     G = GO TO (procedure-name)
     P = PERFORM (procedure-name)
     T = (ALTER) TO PROCEED TO (procedure-name)
     U = USE FOR DEBUGGING (procedure-name)

  Defined   Cross-reference of procedures   References

      212   CLOSE-FILES. . . . . . . . . .  P139
      176   CONVERT-CANADIAN . . . . . . .  P158
      179   CONVERT-EUROS. . . . . . . . .  P161
      182   CONVERT-POUNDS . . . . . . . .  P164
      150   CONVERT-TRANSACTION. . . . . .  P146
      218   GET-DATE-TIME. . . . . . . . .  P122
      185   OPEN-FILES . . . . . . . . . .  P121
      143   PROCESS-TRANSACTIONS . . . . .  P125
  Defined   Cross-reference of programs     References

       22   PCCTRC

          COPY/BASIS cross-reference of text-names, library names and dataset information

  Text-name Library   File name                                    Concat   ISPF statistics
  (Member)  (DDNAME)  (Dataset name)                               Level    Created         Changed

  DTRANS    SYSLIB    PCS.COB.COPYBOOK                               0     2018/06/02   2018/06/05 14:22:19

 Data Division Map
0Data Definition Attribute codes (rightmost column) have the following meanings:
     D = Object of OCCURS DEPENDING    G = GLOBAL                             S = Spanned file
     E = EXTERNAL                      O = Has OCCURS clause                  U = Undefined format file
     F = Fixed-length file             OG= Group has own length definition    V = Variable-length file
     FB= Fixed-length blocked file     R = REDEFINES                          VB= Variable-length blocked file
     X = Unallocated
0Source   Hierarchy and                                    Base      Displacement  Asmblr Data                      Data Def
 LineID   Data Name                                        Locator     Structure   Definition        Data Type      Attributes
     22  PROGRAM-ID PCCTRC------------------------------------------------------------------------------------------------------*
     44   FD CLEANED-TRANSACTIONS. . . . . . . . . . . . . BLF=00001                                 QSAM           FB
     48   1  TRANC-RECORD. . . . . . . . . . . . . . . . . BLF=00001   000000000   DS 57C            Display
     50   FD CONVERTED-TRANSACTIONS. . . . . . . . . . . . BLF=00002                                 QSAM           FB
     54   1  TRANU-RECORD. . . . . . . . . . . . . . . . . BLF=00002   000000000   DS 57C            Display
     56   FD REPT. . . . . . . . . . . . . . . . . . . . . BLF=00003                                 QSAM           FB
     60   1  REPORT-RECORD . . . . . . . . . . . . . . . . BLF=00003   000000000   DS 70C            Display
     63   1  WS-AREA . . . . . . . . . . . . . . . . . . .             000000000   DS 0CL99          Group
     64     2  WS-MSG. . . . . . . . . . . . . . . . . . .             000000000   DS 70C            Display
     65     2  WS-DATE . . . . . . . . . . . . . . . . . .             000000046   DS 0CL8           Group
     66       3  WS-DATE-YY. . . . . . . . . . . . . . . .             000000046   DS 4C             Disp-Num
     67       3  WS-DATE-MM. . . . . . . . . . . . . . . .             00000004A   DS 2C             Disp-Num
     68       3  WS-DATE-DD. . . . . . . . . . . . . . . .             00000004C   DS 2C             Disp-Num
     69     2  WS-TIME . . . . . . . . . . . . . . . . . .             00000004E   DS 0CL8           Group
     70       3  WS-TIME-HH. . . . . . . . . . . . . . . .             00000004E   DS 2C             Disp-Num
     71       3  WS-TIME-MM. . . . . . . . . . . . . . . .             000000050   DS 2C             Disp-Num
     72       3  WS-TIME-SS. . . . . . . . . . . . . . . .             000000052   DS 2C             Disp-Num
     73       3  WS-TIME-HT. . . . . . . . . . . . . . . .             000000054   DS 2C             Disp-Num
     74     2  TRANC-ENDFILE . . . . . . . . . . . . . . .             000000056   DS 1C             Display
     75     88 TRANC-AT-ENDFILE. . . . . . . . . . . . . .
     76     88 TRANC-NOT-AT-ENDFILE. . . . . . . . . . . .
     77     2  TRANC-REC-COUNT . . . . . . . . . . . . . .             000000057   DS 2C             Binary
     78     2  TRANU-REC-COUNT . . . . . . . . . . . . . .             000000059   DS 2C             Binary
     79     2  TRANC-REC-COUNT-D . . . . . . . . . . . . .             00000005B   DS 4C             Disp-Num
     80     2  TRANU-REC-COUNT-D . . . . . . . . . . . . .             00000005F   DS 4C             Disp-Num
     81   1  FILE-STATUS . . . . . . . . . . . . . . . . .             000000000   DS 0CL6           Group
     82     2  TRANC-STATUS. . . . . . . . . . . . . . . .             000000000   DS 0CL2           Group
     83       3  TRANC-STATUS-1. . . . . . . . . . . . . .             000000000   DS 1C             Display
     84       88 TRANC-OK. . . . . . . . . . . . . . . . .
     85       88 TRANC-EOF . . . . . . . . . . . . . . . .
     86       3  TRANC-STATUS-2. . . . . . . . . . . . . .             000000001   DS 1C             Display
     87     2  TRANU-STATUS. . . . . . . . . . . . . . . .             000000002   DS 0CL2           Group
     88       3  TRANU-STATUS-1. . . . . . . . . . . . . .             000000002   DS 1C             Display
     89       88 TRANU-OK. . . . . . . . . . . . . . . . .
     90       88 TRANU-EOF . . . . . . . . . . . . . . . .
     91       3  TRANU-STATUS-2. . . . . . . . . . . . . .             000000003   DS 1C             Display
     92     2  REPORT-STATUS . . . . . . . . . . . . . . .             000000004   DS 0CL2           Group
     93       3  REPORT-STATUS-1 . . . . . . . . . . . . .             000000004   DS 1C             Display
     94       88 REPORT-OK . . . . . . . . . . . . . . . .
     95       88 REPORT-EOF. . . . . . . . . . . . . . . .
     96       3  REPORT-STATUS-2 . . . . . . . . . . . . .             000000005   DS 1C             Display
    102   1  DAILY-TRANSACTION-RECORD. . . . . . . . . . .             000000000   DS 0CL57          Group
    103     2  DTRAN-ACCOUNT-NUMBER. . . . . . . . . . . .             000000000   DS 16C            Disp-Num
    104     2  DTRAN-DATE-YYYYMMDD . . . . . . . . . . . .             000000010   DS 8C             Disp-Num
    105     2  DTRAN-TIME-HHMMSS . . . . . . . . . . . . .             000000018   DS 6C             Disp-Num
    106     2  DTRAN-AMOUNT. . . . . . . . . . . . . . . .             00000001E   DS 10C            Disp-Num
    107     2  DTRAN-TYPE. . . . . . . . . . . . . . . . .             000000028   DS 1C             Display
    108     88 DTRAN-DEBIT . . . . . . . . . . . . . . . .
    109     88 DTRAN-CREDIT. . . . . . . . . . . . . . . .
    110     2  DTRAN-CURRENCY-CODE . . . . . . . . . . . .             000000029   DS 3C             Display
    111     2  DTRAN-RETAILER-CODE . . . . . . . . . . . .             00000002C   DS 6C             Disp-Num
    112     2  DTRAN-AUTH-CODE . . . . . . . . . . . . . .             000000032   DS 6C             Disp-Num
    113     2  DTRAN-CODE. . . . . . . . . . . . . . . . .             000000038   DS 1C             Display
    114     88 DTRAN-PAPER-ENTRY . . . . . . . . . . . . .
    115     88 DTRAN-MANUAL-ENTER. . . . . . . . . . . . .
    116     88 DTRAN-CARD-SWIPED . . . . . . . . . . . . .
    117     88 DTRAN-CHIP-READ . . . . . . . . . . . . . .
-* Statistics for COBOL program PCCTRC:
 *    Source records = 229
 *    Data Division statements = 53
 *    Procedure Division statements = 60
 *    Generated COBOL statements = 0
 *    Program complexity factor = 60
0End of compilation 1,  program PCCTRC,  no statements flagged.
0Return code 0
