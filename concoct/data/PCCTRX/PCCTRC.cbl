      *-----------------------------------------------------------
      *
      * PCCTRC - Phase Change Credit Daily Transaction Edit
      *
      * Program to convert currency to USD in all daily
      * credit card transactions.
      * Converted transactions are written to a file for further
      * processing.
      * A brief report is also written.
      *
      * Alphabetical list of COBOL verbs used:
      * ACCEPT, ADD, CLOSE, COMPUTE, CONTINUE, DISPLAY, EXIT, IF, MOVE,
      * OPEN, PERFORM, READ, SET, STOP, STRING, WRITE
      *
      * Input: TRANC - Input cleaned/verified Transactions
      *
      * Output: TRANU - Output transacions converted to USD
      *         REPT  - Output report
      *
      *-----------------------------------------------------------
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  PCCTRC.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT CLEANED-TRANSACTIONS
                  ASSIGN       TO TRANC
                  ORGANIZATION IS SEQUENTIAL
                  ACCESS MODE  IS SEQUENTIAL
                  FILE STATUS  IS TRANC-STATUS.
           SELECT CONVERTED-TRANSACTIONS
                  ASSIGN       TO TRANU
                  ORGANIZATION IS SEQUENTIAL
                  ACCESS MODE  IS SEQUENTIAL
                  FILE STATUS  IS TRANU-STATUS.
           SELECT REPT
                  ASSIGN       TO REPT
                  ORGANIZATION IS SEQUENTIAL
                  ACCESS MODE  IS SEQUENTIAL
                  FILE STATUS  IS REPORT-STATUS.
      *
       DATA DIVISION.
       FILE SECTION.
       FD  CLEANED-TRANSACTIONS
           RECORD CONTAINS 57 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  TRANC-RECORD           PIC X(57).
      *
       FD  CONVERTED-TRANSACTIONS
           RECORD CONTAINS 57 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  TRANU-RECORD           PIC X(57).
      *
       FD  REPT
           RECORD CONTAINS 70 CHARACTERS
           BLOCK CONTAINS 0 CHARACTERS
           RECORDING MODE IS F.
       01  REPORT-RECORD          PIC X(70).
      *
       WORKING-STORAGE SECTION.
       01  WS-AREA.
           05  WS-MSG             PIC X(70).
           05  WS-DATE.
               10  WS-DATE-YY     PIC 9(4).
               10  WS-DATE-MM     PIC 99.
               10  WS-DATE-DD     PIC 99.
           05  WS-TIME.
               10  WS-TIME-HH     PIC 99.
               10  WS-TIME-MM     PIC 99.
               10  WS-TIME-SS     PIC 99.
               10  WS-TIME-HT     PIC 99.
           05  TRANC-ENDFILE      PIC X.
               88 TRANC-AT-ENDFILE      VALUE 'Y'.
               88 TRANC-NOT-AT-ENDFILE  VALUE 'N'.
           05  TRANC-REC-COUNT    PIC 9(4) COMP VALUE 0.
           05  TRANU-REC-COUNT    PIC 9(4) COMP VALUE 0.
           05  TRANC-REC-COUNT-D  PIC 9(4).
           05  TRANU-REC-COUNT-D  PIC 9(4).
       01  FILE-STATUS.
           05  TRANC-STATUS.
               10  TRANC-STATUS-1      PIC X.
                   88  TRANC-OK   VALUE '0'.
                   88  TRANC-EOF  VALUE '1'.
               10  TRANC-STATUS-2       PIC X.
           05  TRANU-STATUS.
               10  TRANU-STATUS-1      PIC X.
                   88  TRANU-OK   VALUE '0'.
                   88  TRANU-EOF  VALUE '1'.
               10  TRANU-STATUS-2       PIC X.
           05  REPORT-STATUS.
               10  REPORT-STATUS-1     PIC X.
                   88  REPORT-OK  VALUE '0'.
                   88  REPORT-EOF VALUE '1'.
               10  REPORT-STATUS-2      PIC X.
      *
       01  DAILY-TRANSACTION-RECORD.
           05  DTRAN-ACCOUNT-NUMBER        PIC 9(16).
           05  DTRAN-DATE-YYYYMMDD         PIC 9(8).
           05  DTRAN-TIME-HHMMSS           PIC 9(6).
           05  DTRAN-AMOUNT                PIC 9(8)V99.
           05  DTRAN-TYPE                  PIC X.
               88 DTRAN-DEBIT              VALUE 'D'.
               88 DTRAN-CREDIT             VALUE 'C'.
           05  DTRAN-CURRENCY-CODE         PIC XXX.
           05  DTRAN-RETAILER-CODE         PIC 9(6).
           05  DTRAN-AUTH-CODE             PIC 9(6).
           05  DTRAN-CODE                  PIC X.
               88 DTRAN-PAPER-ENTRY        VALUE 'P'.
               88 DTRAN-MANUAL-ENTER       VALUE 'M'.
               88 DTRAN-CARD-SWIPED        VALUE 'S'.
               88 DTRAN-CHIP-READ          VALUE 'C'.

      *
       PROCEDURE DIVISION.
      *
           PERFORM OPEN-FILES.
           PERFORM GET-DATE-TIME.
      *
           SET TRANC-NOT-AT-ENDFILE TO TRUE.
           PERFORM PROCESS-TRANSACTIONS UNTIL TRANC-AT-ENDFILE.
      *
           MOVE 'PCCTRC COMPLETED NORMALLY' TO WS-MSG.
           DISPLAY WS-MSG.
           WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 2 LINES.
           MOVE SPACES TO WS-MSG.
           STRING 'Processed '
                  TRANC-REC-COUNT-D ' Daily Transactions In and '
                  TRANU-REC-COUNT-D ' Daily Transactions Out.'
                  DELIMITED BY SIZE INTO WS-MSG
           END-STRING.
           DISPLAY WS-MSG.
           WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 2 LINES.
      *
           PERFORM CLOSE-FILES.
      *
           STOP RUN.
      *
       PROCESS-TRANSACTIONS.
           READ CLEANED-TRANSACTIONS INTO DAILY-TRANSACTION-RECORD
               AT END SET TRANC-AT-ENDFILE TO TRUE
               NOT AT END PERFORM CONVERT-TRANSACTION
           END-READ.
           EXIT.
      *
       CONVERT-TRANSACTION.
           ADD 1 TO TRANC-REC-COUNT
               GIVING TRANC-REC-COUNT TRANC-REC-COUNT-D.

           IF DTRAN-CURRENCY-CODE = 'USD' THEN
               CONTINUE
           ELSE
               IF DTRAN-CURRENCY-CODE = 'CAD' THEN
                   PERFORM CONVERT-CANADIAN
               ELSE
                   IF DTRAN-CURRENCY-CODE = 'EUR' THEN
                       PERFORM CONVERT-EUROS
                   ELSE
                       IF DTRAN-CURRENCY-CODE = 'GBP' THEN
                           PERFORM CONVERT-POUNDS
                       END-IF
                   END-IF
               END-IF
           END-IF.

           MOVE 'USD' TO DTRAN-CURRENCY-CODE.
           WRITE TRANU-RECORD FROM DAILY-TRANSACTION-RECORD.
           ADD 1 TO TRANU-REC-COUNT
               GIVING TRANU-REC-COUNT TRANU-REC-COUNT-D.
           EXIT.
      *
       CONVERT-CANADIAN.
           COMPUTE DTRAN-AMOUNT ROUNDED = DTRAN-AMOUNT * 1.29514.
      *
       CONVERT-EUROS.
           COMPUTE DTRAN-AMOUNT ROUNDED = DTRAN-AMOUNT * 0.85760.
      *
       CONVERT-POUNDS.
           COMPUTE DTRAN-AMOUNT ROUNDED = DTRAN-AMOUNT * 0.74929.
      *
       OPEN-FILES.
           OPEN INPUT CLEANED-TRANSACTIONS.
           IF NOT TRANC-OK
               STRING 'TRANC OPEN Error - File Status is: '
                      TRANC-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           OPEN OUTPUT CONVERTED-TRANSACTIONS.
           IF NOT TRANU-OK
               STRING 'TRANU OPEN Error - File Status is: '
                      TRANU-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           OPEN OUTPUT REPT.
           IF NOT REPORT-OK
               STRING 'REPORT OPEN Error - File Status is: '
                      REPORT-STATUS DELIMITED BY SIZE INTO WS-MSG
               END-STRING
               DISPLAY WS-MSG
               STOP RUN.
      *
           EXIT.
      *
       CLOSE-FILES.
           CLOSE CLEANED-TRANSACTIONS.
           CLOSE CONVERTED-TRANSACTIONS.
           CLOSE REPT.
           EXIT.
      *
       GET-DATE-TIME.
           ACCEPT WS-DATE FROM DATE YYYYMMDD.
           ACCEPT WS-TIME FROM TIME.
           MOVE SPACES TO WS-MSG.
           STRING 'PCCTRC Started on '
                  WS-DATE-YY '/' WS-DATE-MM '/' WS-DATE-DD ' at '
                  WS-TIME-HH ':' WS-TIME-MM ':' WS-TIME-SS
                  DELIMITED BY SIZE INTO WS-MSG
           END-STRING.
           DISPLAY WS-MSG.
           WRITE REPORT-RECORD FROM WS-MSG AFTER ADVANCING 1 LINE.
           EXIT.
