"""
This Program is used to compute a lexical to compare more sophisticated matching algorithms.
"""

from __future__ import print_function
import os
import sys
from pathlib import Path
from ipdb import set_trace

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))

if root not in sys.path:
    sys.path.append(str(root))

from utils.slicing.slicing import Slice
from data.ontologies.onto_io import OntoIO
from utils.nlp.similarity import phrase_sentence_distance

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


def map_slice_to_ontology(slice):
    """
    Maps the contents of a source_code to concepts in the ontology.

    Parameters
    ----------
    slice: dict
        A dictionary containing source_code information.

    Return
    ------
    maps: dict
        Top N (N=5) ontological concepts that match the given variable names or
        perform statement names
    """

    onto_io = OntoIO(filename=root.joinpath("data", "ontologies", "SIMBOL.rdf"))

    # Get all variable names from a source_code
    var_labels = slice["variable_names"]

    # Get all statement names from a source_code
    stmt_labels = slice["perform_stmt_label"]

    # Get all the ontology concepts as {'label': 'definition'}
    ontology_definitions_and_labels = onto_io.get_definitions_and_labels()
    concepts = dict()
    "Match statements to concepts"
    for labels in stmt_labels:
        stmt_concept = list()
        for key, val in ontology_definitions_and_labels.items():
            stmt_concept.append((key, phrase_sentence_distance(labels, val)))
        stmt_concept = sorted(stmt_concept, key=lambda X: X[1])[:5]
        concepts.update({labels: [stmt[0] for stmt in stmt_concept]})

    return concepts


def main(verbose=False):
    """
    Get all cobol slices from the control flow graphs, then identify the PERFORM statements
    and their corresponding labels.

    Parameters
    ----------
    N/A: None
        This method takes no input parameters

    Returns
    -------
    slice_info: dict
        This is a (possibly) nested dictionary of source_code information

    Notes
    -----

    So, lets say we have 2 slices. The return would look as follows:

    slice_info = {
        "slice_1":
            {
                "variable_names": ...,
                "variable_types": ...,
                "statement_types": ...,
                "perform_stmt_label": ...
            }
        "slice_2":
            {
                "variable_names": ...,
                "variable_types": ...,
                "statement_types": ...,
                "perform_stmt_label": ...
            }
    }

    """
    slicer = Slice()
    slices = slicer.get_slice_from_file(filename=root.joinpath("data", "slices", "PCCTRC.txt"))
    slice_info = dict()
    set_trace()

    for n, slice in enumerate(slices):
        vars = []
        var_types = []
        stmt_types = []
        perform_lbl = []
        descriptions = []

        for statement in slice.statements:
            "Store all variables"
            vars.extend(statement.all_variables)
            "Store statement types"
            stmt_types.extend(statement.statement_type)
            "Check is there are perform statements"
            if statement.perform_tag:
                perform_lbl.append(statement.perform_tag)
            "Finally, set variable types"
            var_types.extend(statement.variables_type)
            descriptions.append(statement.description)


        slice_info.update({
            "slice_{}".format(n): {
                "variable_names": list(set(vars)),
                "variable_types": list(set(var_types)),
                "statement_types": list(set(stmt_types)),
                "is_perform_stmt": bool(len(perform_lbl)),
                "perform_stmt_label": list(set(perform_lbl))
            }
        })

    set_trace()

    for slice_name, slice_details in slice_info.items():
        slice_info.update({slice_name: map_slice_to_ontology(slice_details)})

    if verbose:
        for key, val in slice_info.items():
            if val:
                print(key)
                for key2, val2 in val.items():
                    print("\t+--{}: ".format(key2), end=" ")
                    print("".join(["<{}> ".format(kw) for kw in val2]))

    return slice_info


if __name__ == "__main__":
    main()
