import os
import re
import sys
import unittest
from ipdb import set_trace
from pathlib import Path

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"

from utils.nlp.noisy_channel.model import LanguageNgramModel, MissingLetterModel
from utils.nlp.noisy_channel.noisy_channel import decipher


class TestNoisyChannel(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestNoisyChannel, self).__init__(*args, **kwargs)
        self.lang_model = LanguageNgramModel(1)
        self.lang_model.fit(" abracadabra ")
        self.missed_model = MissingLetterModel(0)
        self.missed_model.fit([("abracadabra", "abr_c_d_br_")])

    def test_build_language_model(self):
        next_letters = self.lang_model.predict_proba(" bra")
        self.assertAlmostEquals(next_letters['b'], 0.2786, places=2)

    def test_smart_abbrv(self):
        with open("tests/test_inputs/PCCTRX.txt", encoding='utf-8') as f:
            text = f.read()
        text = re.sub(r'[^a-z ]+', '', text.lower().replace('\n', ' '))
        lang_model = LanguageNgramModel(order=4, smoothing=0.001, recursive=0.01)
        lang_model.fit(text)
        abbrv = lang_model.generate_smart_abbreviations()
        self.assertIsInstance(abbrv, list)
        self.assertGreater(len(abbrv), 0)

    def test_build_missing_letter_model(self):
        self.assertAlmostEquals(self.missed_model.predict_proba('abr', 'a'), 0.7166, places=2)

    def test_get_probability_of_abbrv(self):
        self.assertAlmostEquals(self.missed_model.single_proba("", 'abra', 'abr_'), 0.1645, places=2)

    def test_decipher(self):
        result = decipher('brc', self.lang_model, self.missed_model, verbose=False, freedom=1)
        self.assertTrue('brc' in result.keys())
        self.assertTrue('brac' in result.keys())
        self.assertTrue('abrc' in result.keys())

    def test_on_hobbits(self):
        with open("tests/test_inputs/tolkein.txt", encoding='utf-8') as f:
            text = f.read()
        text = re.sub(r'[^a-z ]+', '', text.lower().replace('\n', ' '))
        all_letters = ''.join(list(sorted(list(set(text)))))
        missing_set = (
                [(all_letters, '-' * len(all_letters))] * 3  # all chars missing
                + [(all_letters, all_letters)] * 10  # all chars are NOT missing
                + [('aeiouy', '------')] * 30  # only vowels are missing
        )
        # Train the both models
        big_lang_m = LanguageNgramModel(order=4, smoothing=0.001, recursive=0.01)
        big_lang_m.fit(text)
        big_err_m = MissingLetterModel(order=1, smoothing_missed=0.1)
        big_err_m.fit(missing_set)

        word = decipher('frod', big_lang_m, big_err_m)
        self.assertTrue('frodo' in word.keys())
        word = decipher('sm', big_lang_m, big_err_m)
        self.assertTrue('sam' in word.keys())
        word = decipher('batl', big_lang_m, big_err_m)
        self.assertTrue('battle' in word.keys())
        word = decipher('wtr', big_lang_m, big_err_m)
        self.assertTrue('water' in word.keys())

    def test_pcctrx(self):
        with open("tests/test_inputs/PCCTRX.txt", encoding='utf-8') as f:
            text = f.read()
        text = re.sub(r'[^a-z ]+', '', text.lower().replace('\n', ' '))

        lang_model = LanguageNgramModel(order=4, smoothing=0.001, recursive=0.01)
        lang_model.fit(text)
        abbrv_model = MissingLetterModel(order=1, smoothing_missed=0.1)

        missing_set = lang_model.generate_smart_abbreviations()

        abbrv_model.fit(missing_set)

        set_trace()

        word = decipher('msg', lang_model, abbrv_model, optimism=0.75)
        self.assertTrue('message' in word.keys())

        word = decipher('trans', lang_model, abbrv_model, optimism=0.75)
        self.assertTrue('transaction' in word.keys())

        word = decipher('dtran', lang_model, abbrv_model, optimism=0.75)
        self.assertTrue('transaction' in word.keys())

        word = decipher('addr', lang_model, abbrv_model, optimism=0.75)
        self.assertTrue('address' in word.keys())

        word = decipher('crd', lang_model, abbrv_model, optimism=0.75)
        self.assertTrue('credit' in word.keys())

        word = decipher('dbt', lang_model, abbrv_model, optimism=0.75)
        self.assertTrue('debit' in word.keys())


if __name__ == '__main__':
    unittest.main()
