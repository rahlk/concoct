from __future__ import print_function
import os
import sys
from pathlib import Path
import unittest
import networkx as nx
from random import randint

from ipdb import set_trace

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from utils.source_code.slicing import Slice
from utils.data_structures.metamodel import BasicBlock

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


class TestSlice(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestSlice, self).__init__(*args, **kwargs)
        self.slice = Slice(dot_file=os.path.join(root, "data/control_flow_graphs/PCCTRE.dot"))

    def test_dot_to_netwx(self):
        graph = self.slice._dot_to_nx()
        self.assertIsInstance(graph, nx.classes.multidigraph.MultiDiGraph)

    def test_get_slice_from_file(self):
        file = root.joinpath("data", "slices", "PCCTRC.txt")
        slices = self.slice.get_slice_from_file(file)
        self.assertIsInstance(slices, list)
        self.assertIsInstance(slices[randint(0, len(slices))], BasicBlock)

    def test_get_sink_nodes(self):
        graph = self.slice._dot_to_nx()
        self.slice._get_sink_and_source_nodes(graph)

    def test_get_slices(self):
        self.slice = Slice(dot_file=os.path.join(root, "data/control_flow_graphs/PCCTRC.dot"))
        graph = self.slice._dot_to_nx()
        slices = self.slice.get_slice(graph)
        "PRINT SLICES here"
        f = open(os.path.join(Path(root).parents[0], "tests", "test_outputs", "PCCTRC_slices.txt"), "w+")
        for ss in slices:
            print("Slice {}".format(hex(id(ss))), file=f)
            for i, bb in enumerate(ss):
                for stat in bb.statements:
                    print("\t+---", " ".join(stat.cobol_statement), file=f)
            print(80 * "=", file=f)


if __name__ == "__main__":
    unittest.main()
