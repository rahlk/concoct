from __future__ import print_function
import os
import sys
from pathlib import Path
import unittest
from ipdb import set_trace

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from utils.source_code.vartree import VarHierarchy

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


class TestVarTree(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestVarTree, self).__init__(*args, **kwargs)
        self.vh = VarHierarchy(cobol_file=root.joinpath("data", "PCCTRX", "PCCTRC.cbl"))

    def test_getting_raw_cobol_script(self):
        self.assertIsInstance(self.vh.raw_script, list)
        self.assertIsInstance(self.vh.raw_script[0], str)

    def test_working_storage_section(self):
        ws_vars = self.vh.working_storage_section()
        self.assertIsInstance(ws_vars, list)

    def test_generate_var_tree(self):
        var_tree = self.vh.generate_var_tree()
        self.assertEqual(var_tree.__repr__(), "'Working Storage Section'\n\t'WS-AREA.'\n\t\t'WS-MSG'\n\t\t'WS-DATE.'\n\t\t\t'WS-DATE-YY'\n\t\t\t'WS-DATE-MM'\n\t\t\t'WS-DATE-DD'\n\t\t'WS-TIME.'\n\t\t\t'WS-TIME-HH'\n\t\t\t'WS-TIME-MM'\n\t\t\t'WS-TIME-SS'\n\t\t\t'WS-TIME-HT'\n\t\t'TRANC-ENDFILE'\n\t\t\t'TRANC-AT-ENDFILE'\n\t\t\t'TRANC-NOT-AT-ENDFILE'\n\t\t'TRANC-REC-COUNT'\n\t\t'TRANU-REC-COUNT'\n\t\t'TRANC-REC-COUNT-D'\n\t\t'TRANU-REC-COUNT-D'\n\t'FILE-STATUS.'\n\t\t'TRANC-STATUS.'\n\t\t\t'TRANC-STATUS-1'\n\t\t\t\t'TRANC-OK'\n\t\t\t\t'TRANC-EOF'\n\t\t\t'TRANC-STATUS-2'\n\t\t'TRANU-STATUS.'\n\t\t\t'TRANU-STATUS-1'\n\t\t\t\t'TRANU-OK'\n\t\t\t\t'TRANU-EOF'\n\t\t\t'TRANU-STATUS-2'\n\t\t'REPORT-STATUS.'\n\t\t\t'REPORT-STATUS-1'\n\t\t\t\t'REPORT-OK'\n\t\t\t\t'REPORT-EOF'\n\t\t\t'REPORT-STATUS-2'\n")


if __name__ == "__main__":
    unittest.main()
