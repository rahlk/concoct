import os
import sys
import unittest

"Update python path"
root = os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct'))
if root not in sys.path:
    sys.path.append(root)

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


class TestHybrid(unittest.TestCase):

    def test_main(self):
        pass


if __name__ == "__main__":
    unittest.main()
