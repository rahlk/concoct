import os
import sys
import unittest
from pathlib import Path
from ipdb import set_trace

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from utils.misc.process_dictionary import DictionaryParse

class TestProcessDictionary(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestProcessDictionary, self).__init__(*args, **kwargs)
        self.pdf = DictionaryParse(pdf_file=str(root.joinpath("data", "dictionary", "business_dictionary.txt")))

    def test_extract_tokens(self):
        tokens = [tok for tok in self.pdf.extract_tokens()]
        set_trace()
        # print("\n".join(tokens), file=open(root.joinpath("data", "dictionary", "business_dictionary.txt"), "w+"))



if __name__ == "__main__":
    unittest.main()