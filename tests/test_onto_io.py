import os
import re
import sys
import rdflib
import unittest
import networkx as nx
from pathlib import Path

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from data.ontologies import onto_io

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"

RDF_FILE = root.joinpath("data", "ontologies", "SIMBOL.rdf")
ONTO_IO = onto_io.OntoIO(filename=RDF_FILE)


class TestOntoIO(unittest.TestCase):
    def test_read(self):
        g = ONTO_IO.read()
        self.assertIsInstance(g, rdflib.graph.Graph)

    def test_convert_to_networkx(self):
        graph = ONTO_IO.read()
        g = onto_io.OntoIO.rdflib_graph_to_networkx(graph)
        self.assertIsInstance(g, nx.classes.multidigraph.MultiDiGraph)

    def test_write_to_file(self):
        g = ONTO_IO.read()
        onto_io.OntoIO.write_to_file(g, savename="test.ttl", savetype='turtle')
        os.remove("test.ttl")

    def test_write_as_image(self):
        ONTO_IO.write_as_image()
        os.remove(re.sub(".rdf", ".dot", str(RDF_FILE)))

    def test_get_onto_root_definitions_and_labels(self):
        onto_dict = ONTO_IO.get_definitions_and_labels()
        self.assertIsInstance(onto_dict, dict)


if __name__ == '__main__':
    unittest.main()
