import os
import sys
import unittest
from pathlib import Path

"Update python path"
root = Path(os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct')))
if root not in sys.path:
    sys.path.append(str(root))

from utils.nlp.guesstimate import Simple, NoisyChannelEstimation
from utils.nlp.similarity import phrase_sentence_distance


class TestNLP(unittest.TestCase):
    def test_simple_full_word_guess(self):
        abbrv = "REPT"
        guess = Simple.full_word_guess(abbrv)
        self.assertEqual(guess.lower(), "report")

    def test_noisy_channel_word_guess(self):
        abbrv = "REPT"
        nce = NoisyChannelEstimation()
        guess = nce.full_word_guess(abbrv)

    def test_phrase_sentence_distance(self):
        sentence = "daily transactions"
        phrase = "DAILY-TRANC"
        dist = phrase_sentence_distance(phrase, sentence)
        self.assertLess(dist, 0.5)


if __name__ == "__main__":
    unittest.main()