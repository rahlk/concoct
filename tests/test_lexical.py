import os
import sys
import unittest


"Update python path"
root = os.path.abspath(os.path.join(
    os.getcwd().split("concoct")[0], 'concoct/concoct'))
if root not in sys.path:
    sys.path.append(root)

import lexical.match as lexical_match

__author__ = "Rahul Krishna (i.m.ralk@gmail.com)"


class TestBaseline(unittest.TestCase):

    def test_main(self):
        slice_info = lexical_match.main(verbose=False)
        self.assertGreater(len(slice_info), 0)


if __name__ == "__main__":
    unittest.main()
