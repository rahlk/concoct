"""
Compare procedure names and variable names
"""

from __future__ import print_function
import os
import sys
from pathlib import Path
from ipdb import set_trace
from collections import defaultdict
from numpy import isnan

"Update python path"
root = Path(os.path.abspath(os.path.join(
        os.getcwd().split("concoct")[0], 'concoct/concoct')))

if root not in sys.path:
    sys.path.append(str(root))

from utils.slicing.slicing import Slice
from data.ontologies.onto_io import OntoIO
from utils.nlp.guesstimate import NoisyChannelEstimation
from utils.nlp.similarity import phrase_sentence_distance


def run_experiment_2():
    """
    Compare procedure names only
    """

    slicer = Slice()
    slices = slicer.get_slice_from_file(
        filename=root.parent.joinpath("experiments", "experiment_data", "slices", "slice.txt"))
    onto_a = OntoIO(filename=root.parent.joinpath("experiments", "experiment_data", "ontologies", "SIMBOL.rdf"))
    onto_b = OntoIO(filename=root.parent.joinpath("experiments", "experiment_data", "ontologies", "FRONT.rdf"))
    onto_c = OntoIO(filename=root.parent.joinpath("experiments", "experiment_data", "ontologies", "InterestRates.rdf"))

    # Get all statement names from a source_code
    est = NoisyChannelEstimation()
    slice_info = defaultdict(lambda: [])
    for n, slice in enumerate(slices):
        all_vars = []
        for statement in slice.statements:
            all = statement.get_all_variables()
            for each in all:
                all_vars.append(" ".join(est.full_word_guess(each)))
        slice_info["slice{}".format(n + 1)] = list(set(all_vars))


    "Get all the ontology concepts as {'label': 'definition'}"
    ontology_definitions_and_labels_1 = onto_a.get_definitions_and_labels()
    ontology_definitions_and_labels_2 = onto_b.get_definitions_and_labels()
    ontology_definitions_and_labels_3 = onto_c.get_definitions_and_labels()

    slice_details = defaultdict(lambda: defaultdict(lambda: []))
    for slice_name, slice_performs in slice_info.items():
        concepts = defaultdict(lambda: [])
        "Match statements to concepts"
        for labels in slice_performs:
            stmt_concept_1 = list()
            stmt_concept_2 = list()
            stmt_concept_3 = list()

            for key, val in ontology_definitions_and_labels_1.items():
                stmt_concept_1.append((key, phrase_sentence_distance(labels, val)))
            stmt_concept_1 = sorted(stmt_concept_1, key=lambda X: X[1])[:5]
            stmt_concept_1 = [concept for concept in stmt_concept_1 if not isnan(concept[1])]
            concepts["SIMBOL"].append([(stmt[0], int((1 - stmt[1]) * 100)) for stmt in stmt_concept_1])

            for key, val in ontology_definitions_and_labels_2.items():
                stmt_concept_2.append((key, phrase_sentence_distance(labels, val)))
            stmt_concept_2 = sorted(stmt_concept_2, key=lambda X: X[1])[:5]
            stmt_concept_2 = [concept for concept in stmt_concept_2 if not isnan(concept[1])]
            concepts["FRONT"].append([(stmt[0], int((1 - stmt[1]) * 100)) for stmt in stmt_concept_2])

            for key, val in ontology_definitions_and_labels_3.items():
                stmt_concept_3.append((key, phrase_sentence_distance(labels, val)))
            stmt_concept_3 = sorted(stmt_concept_3, key=lambda X: X[1])[:5]
            stmt_concept_3 = [concept for concept in stmt_concept_3 if not isnan(concept[1])]
            concepts["INRON"].append([(stmt[0], int((1 - stmt[1]) * 100)) for stmt in stmt_concept_3])
        slice_details[slice_name] = concepts

    set_trace()

if __name__ == "__main__":
    run_experiment_2()
